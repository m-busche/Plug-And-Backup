Title: Help
Published: 03/05/2017
Tags: ['Help', 'Support', 'FAQ']
---

# Support
If you consider any problems with *Plug-And-Backup*, first have a look at the
[documentation](/documentation). If you don't find your answer, please file
an issue at the project's [Gitlab site](https://gitlab.com/m-busche/Plug-And-Backup/issues).

Support requests by email will probably not be answered, so **please** use the issue tracker.

# Updates
Before asking questions or struggeling with a problem: Use *Plug-And-Backup's* update
functionailty (`Help` - `Check for update`) to be sure you are using the latest version
or check the [download page](/download) for updates.

# Licensing
*Plug-And-Backup* is licensed under the MIT license. The software uses some third party components. 
For licensing details, see `Help` - `About and licensing`.

# FAQ
`[no questions asked, yet]`