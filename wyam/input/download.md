Title: Download
Published: 03/05/2017
Tags: ['Download', 'Backup', 'Windows']
---

# Download Plug-And-Backup

Download the latest version [here](https://plug-and-backup.eu/download/download.php?Plug-And-Backup_Setup.exe) (v1.6.0 05052017)

# Changelog

## Version 1.6.0
- Some german translation in `Plug-And-Backup-Restore.exe`
- Changed email report text and formatting
- Some documentation added
- Fixed a bug concerning sending mail report to only one receipient
- Fixed setting `waitbeforestart` value
- Optional delete lock file (Jobrunner, requires user input)
- File counts and sizes in report
- Fixed 7-Zip command line bug in `Plug-And-Backup-Restore.exe`

## Version 1.5.4
- Donation links (Paypal, Flattr)
- Pre- and Post backup commands (via INI file)
- Secondary log file on target device
- Email reporting

## Version 1.5.3
- Display Description, Last Run and Last Status in Backup Manager
- Added Plug-And-Backup-Restore.exe on backup device (English version)
  - Restore all files
  - Start 7-Zip File Manager
  - Test archive integrity
 
## Version 1.5.2
  - Fixed problem with 1.5.1 auto update

## Version 1.5.1
 - Fixed german language problem in Main Form
 - Added changelog menu item

## Version 1.5
 - First release

