Title: Plug-And-Backup released
Published: 03/05/2017
Tags: ['Software', 'Backup', 'Windows']
---
After several weeks of work on my weekends, my new Windows Tool *Plug-And-Backup* is released
to the public.

The idea for this tiny backup solution came as my daughter lost all her pictures from her notebook
a few months ago. I was looking for a really simple and easy to use backup tool for really 
lazy people.

So I searched for a tool that was able to start a backup without any user interaction just by
plugging a USB thumb drive (or something similar) into the computer.

As the whole internet seems to be flooded with Windows backup tools, I wasn't able to find one
that matched my criteria *and* was free and open sourced.

So the idea developed into the plan to write my own backup tool. *Plug-And-Backup* was born.

# What makes Plug-And-Backup different?

- Maximum ease of use: Just plug your backup device into your PC to make a backup. That's all.
- Standard technologies, no proprietary file formats: *Plug-And-Backup* generates 7-Zip files
which can be restored on quite any device without installing *Plug-And-Backup* itself.
- Don't think about a backup strategy. If *Plug-And-Backup* doesn't find a former backup, it makes
a full backup. If it does, it makes a differential one.
- Reliability: *Plug-And-Backup* creates log files, which can automatically be sent via email.
No SMTP server or complicated settings are required (thanks to [Mailgun](https://mailgun.com)).
If something went wrong, you'll immediately know about it.
- Easy to use GUI interface for creating new backup sets. Let your device be detected, drag 
and drop one or more folders to the list, save & go.
- A simple recovery tool tool (along with a copy of the 7-Zip portable version) will be copied
to every backup device. You don' t need more than the backup device in your pocket to recover 
your data.
- Secure your backups with a password (optional).