Title: Documentation moved
Published: 04/05/2017
Tags: ['Documentaion', 'News']
---
Today the project's documentaion was moved from [Gitlab´s](https://gitlab.com/m-busche/Plug-And-Backup/) README.md to this site. See [Documentation](/documentation).

The [Downloads](/download) are now hosted from this server, too.

Hard linked menu items in *Plug-And-Backup* will be changed with the next version update.

Also, I've added [Shariff](https://github.com/heiseonline/shariff) social media share buttons under each blog post. 
They respect your privacy and don't send any data to Facebook, Google & Twitter unless you actively click on them.
