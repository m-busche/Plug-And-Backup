Title: Screen casts arrived
Published: 07/05/2017
Tags: ['Documentation', 'News']
---

Today I had some time to create three screen cast videos explaining the work of
*Plug-And-Backup*.

For casting i used Nicke Manarin's [ScreenToGif](http://www.screentogif.com/).
Thanks a lot for this, Nicke!

The screen casts are located in the [About Plug-And-Backup](/about) section.
