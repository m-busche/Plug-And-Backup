Title: Documentation
Published: 03/05/2017
Tags: ['Documentation', 'Readme', 'Handbook']
---

# Plug-And-Backup

A tiny instant solution for easy backups on USB flash drives

<!-- TOC -->

- [Plug-And-Backup](#plug-and-backup)
    - [Intention](#intention)
    - [Installation](#installation)
    - [Usage](#usage)
        - [Starting Plug-And-Backup](#starting-plug-and-backup)
        - [Set Up A New Backup Device](#set-up-a-new-backup-device)
        - [Run A Backup](#run-a-backup)
        - [Manage Backup Sets](#manage-backup-sets)
        - [Restore A Backup](#restore-a-backup)
        - [Checking Logfiles](#checking-logfiles)
    - [Update](#update)
    - [Tweaking (INI file options)](#tweaking-ini-file-options)
        - [Send Email reports](#send-email-reports)
        - [Mailgun](#mailgun)
        - [WaitForUserBeforeStart](#waitforuserbeforestart)
        - [PreCmd and PostCmd](#precmd-and-postcmd)
        - [Pre/Post Command Wait Interval](#prepost-command-wait-interval)
    - [References](#references)
    - [Into The Deep](#into-the-deep)
    - [Licensing](#licensing)
    - [Bug Reports, Issues](#bug-reports-issues)
    - [Contacting The Author](#contacting-the-author)
    - [Support The Project](#support-the-project)
    - [What is to come next?](#what-is-to-come-next)

<!-- /TOC -->

## Intention
*Plug-And-Backup* is a tiny Windows tool which lets you backup one or more directories to a specific USB thumb drive or USB attachable hard disk.

The backup will be started automatically as soon as you plug the device into the USB port. It will be compressed with 7-Zip and
you can protect it with a password.

*Plug-And-Backup* creates differential backups. Only changed, added or removed files will be processed after the
first full backup is run.

*Plug-And-Backup* is designed to be reliable and easy to use, so don't expect any extraordinary features.

## Installation
Download the latest version of [*Plug-And-Backup*](/Download) and 
install it to a folder of your choice.

## Usage
*Plug-And-Backup* guides you with a step-by-step wizard through the process of creating a new backp set:

### Starting Plug-And-Backup
Start *Plug-And-Backup* using the Windows Start menu.

### Set Up A New Backup Device
A backup sets consist of a (USB-) device and one or more directories to be backed up. *Plug-And-Backup* leads you through the process
of creating a new backup set in a few steps. Follow the instructions at the top of the window.


|   ![Step 0](/screenshots/en/Plug-And-Backup_Step0.png)        |   ![Step 1](/screenshots/en/Plug-And-Backup_Step1.png)           |   ![Step 2](/screenshots/en/Plug-And-Backup_Step2.png)   |
|   ---   |   ---   |   ---   |
|   ![Step 3](/screenshots/en/Plug-And-Backup_Step3.png)        |   ![Step 4](/screenshots/en/Plug-And-Backup_Step4.png)           |   ![Step 5](/screenshots/en/Plug-And-Backup_Step5.png)   |


The steps in detail:
  - Step 0: Introduction screen. (Click `Next`)
  - Step 1: Remove the backup device if it is plugged in. (Click `Next`)
  - Step 2: Now, plug in the device on which the backup shall be written. (Click `Next`).
  - Step 3: The device ID should be displayed. (Click `Next`)
  - Step 4: Select the directories you want to be backed up. Add folders with the `Open Directory` dialog (`...`) and add them
  to the list (or drag and drop directories from Windows Explorer to the list element in the middle of the window). Optional: 
  type a description or set a password. (Click `Activate`)
  - Step 5 (optinal): Click `Back to the start` to create another backup set.
  
By clicking `Activate` in step 4 the backup set is ready to launch.

Notice: Setting a password will protect you from others being able to extract files from your backup device in the case that
you loose the device. Nevertheless the password is stored in plain text on the harddisk of your PC and will be written to 
the log files. So, be aware of this.
  
### Run A Backup
Just plug the corresponding USB device into your computer. The backup should start after Windows has detected the device and assigned
a drive letter.

### Manage Backup Sets
Click on `File` - `Manage backups` to view your backup sets. You can delete a backup set by right clicking on it on the left side.

![Manage Backups](/screenshots/en/Manage_Backups.png)

Removing or adding of single directories in a backup set is not (yet) supported.

### Restore A Backup
**Test the restoration process before relying on any backup!**

You can restore a *Plug-And-Backup* backup on any PC using 7-Zip (*Plug-And-Backup* itself has not to be installed).
*Plug-And-Backup* copies a portable version of 7-Zip to the root directory of each backup device.

Furthermore, your can restore a complete backup with the *Plug-And-Backup*-Restore tool, which is stored
in the root directory of the backup device:

![Plug-And-Backup-Restore](/screenshots/en/Restore-tool.png)

Either 
 - select one of the backup sets, enter a target directory for the extractiion and click `Restore all files in all archives`
(which will restore the first full backup file and all further differential backups). If your backup ist password protected,
you will be asked to enter your password in the 7-Zip command window. Or
 - select one of the backup sets and click on `Start 7-Zip File Manager` to select certain files from the archive
 using 7-Zip Filemanager.

It is recommended to restore the archive into an empty folder to prevent accidentally overwriting files!

### Checking Logfiles
Click on `File` - `Show log files` to start Windows Explorer at the *Plug-And-Backup* log directory. Inspect the latest log files
with a text editor of your choice.

Log files will be cleaned up after seven days.

## Update
*Plug-And-Backup* can easily be updated to the latest version. Click on `Help` - `Check for update` to download the
latest version of the software.

## Tweaking (INI file options)
*Plug-And-Backup* supports some further settings by editing its INI file. Click on `Tools` - `Edit PaB.ini` to open the
file in your text editor.

### Send Email reports
To let *Plug-And-Backup* send reports by email, set `SendReport=True` and add one or more email adresses to `SendReportTo`.


Example:
```
[settings]
SendReport=True
SendReportTo=me@example.com
SendReportTo=you@example.com
```

*Plug-And-Backup* will attach the log file to each email report.

### Mailgun
*Plug-And-Backup* takes use of [Mailguns](https://www.mailgun.com/) API as email provider.
The authors' account is limited to 10.000 mails per day. If this limit is exeeded, no further mail will be sent.

So you are strongly encouraged to use your personal Mailgun Acccount, which you can register 
[here](https://app.mailgun.com/new/signup/). At the time of this writing, a free account is limited to 10.000 mails per day.

**There is another reason to use a personal Mailgun account: Mailgun logs the details of each mail to let the user keep track
of his mails. So, if you don't want the author to read your backup reports: Use your own account!**

After registering a Mailgun account, have a look at yout Dashboard and copy your secret API key from the right:

![Mailgun Dashboard](/screenshots/Mailgun.png)

Edit yout `PaB.ini` file and copy the API key to `MailgunAPIKey=` under `Settings`.
Go back to your Mailgun Dashboard and copy your domain name (in the lower). Paste it to `MailgunDomain=`.

**Example INI Section:**

```
[settings]
SendReport=True
MailgunAPIKey=key-65d52cc570230f0f4e25684c2b2b7238
MailgunDomain=sandbox2c45884527824533b2ccaae8ded609db.mailgun.org
MailgunSender=Plug-And-Backup <noreply@butenostfreesen.de>
SendReportTo=m.busche@gmail.com
```


### WaitForUserBeforeStart
Set the value of `WaitForUserBeforeStart` to the time interval in seconds to wait for starting the backup job(s),
which gives the user some time to cancel the process by hitting Crtl-c.

Set the value to zero (`0`) if you want the job(s) to be startet immediately.

### PreCmd and PostCmd
Each backup device section (`[Device_XYZ]`) has a key for a pre and post backup command.

The value of PreCmd will be executed *before* the execution of the backup.
The value of PostCmd will be executed *after* the execution of the backup.

The result of the process execution will be written to the log file.

**Example useage:**
Stop a Database service to unlock and backup its files, start it after the backup:

```
[Device_070746DA13365D39]
Dir=C:\Users\user\database\mydatabase
Lastrun1=25.04.2017 18:29:31
Laststate1=0 - OK
Password=
Description=Database Backup
PreCmd=net.exe stop myDatabaseServiceName
PostCmd=net.exe start myDatabaseServiceName
```

Don't forget to define a maximum wait time for the commands in the section `Settings` by setting a value for `PrePostCommandWait` (in seconds).

Caution: If you set `PrePostCommandWait=0`, *Plug-And-Backup* will wait until the process ends, which could under certain circumstances
lead to an endless loop.

Hint: If you want to start a Windows command like `copy` or `move`, set it to something like `cmd.exe /c <your command line goes here>` to let it be executed 
by `cmd.exe`.

Notice: Pre- and post commands are executed under the system account that runs the service `PlugAndBackupService`.

### Pre/Post Command Wait Interval
Set the maximum time interval to wait for Pre- and PostCmd to terminate (in seconds). For details, see [above](#precmd-and-postcmd)

Example: `PrePostCommandWait=10`

## References
See `Help` - `About and licensing` for version- and licensing information.

## Into The Deep
*Plug-And-Backup* consists of several different executables:

 - (A) `Plug-And-Backup.exe`: User interface for creating and managing backup sets
 - (B) `Plug-And-Backup-Service.exe`: A Windows service which reacts to USB device insertion events and hands them to
 - (C) `Plug-And-Backup-Jobrunner.exe`: Gets device informations from (B) and starts the backup process
 - (D) `Plug-And-Backup-Restore.exe`: A simple user interface for restoring files, will be copied to each backup device.

 Backup definitions and configuration options are stored in a Windows INI file `PaB.ini` which is
 located at `%ProgramData%\Plug-And-Backup\PaB.ini`. You can manually edit the file. The easiest way 
 is to use (A) and click on `Tools` - `Edit PaB.ini`.

 Log files are created on each backup device when a job is run. Furthermore, *Plug-And-Backup* logs its 
 acitivities to two different log files in `%ProgramData%\Plug-And-Backup\logs`:
  - `Jobrunner_<Date>.log`: Output from (C)
  - `Service_<Date>.log`: Output from (B)

Start (A) and click on `File` - `View log files` to navigate to the log directory and open a log file.

All *local* log files are cleaned up (deleted) after seven days from (B).

## Licensing
This software is licensed as open source software under the MIT license. See `Help` - `About and licensing`.

*Plug-And-Backup* takes use of some third party libraries and software. See `Help` - `About and licensing` for
details about the used components and their corresponding licenses.

## Bug Reports, Issues
Bug reports or other issues will only be accepted if they are filed on the [Project's GitLab site](https://gitlab.com/m-busche/Plug-And-Backup/issues).

## Contacting The Author
If you have any comments regarding this software, let me know! You can contact the author by
email (pab@butenostfreesen.de). Any english or german mail will be at least read.

Note, that bug reports are only accepted as Issues on the Gitlab project site (see above).

## Support The Project
If you like to support this project and its further development, you can
 - send the author a mail and tell him about your experiences with *Plug-And-Backup*
 - spread the word about *Plug-And-Backup*: tell your friends, write a blog artice or post someting on Twitter or Facebook. 
   Don`t forget to mention the author if you twitter something (@plugandbackup).
 - send a small amount of money via Flattr or Paypal.

Flattr here: [![Flattr](https://button.flattr.com/flattr-badge-large.png)](https://flattr.com/submit/auto?fid=glgg90&url=https%3A%2F%2Fgitlab.com%2Fm-busche%2FPlug-And-Backup)

or Paypal there: [![Paypal](https://www.paypalobjects.com/de_DE/DE/i/btn/btn_donate_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=X4BV838GA4RN2)


## What is to come next?
Have a look at the [Trello board](https://trello.com/b/5ws8fJ5C/plug-and-backup) to see what will probably come up 
in one of the next versions.
