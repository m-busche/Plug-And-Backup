Title: About Plug-And-Backup
---

# About Plug-And-Backup

<!-- TOC -->

- [About Plug-And-Backup](#about-plug-and-backup)
    - [What makes Plug-And-Backup different?](#what-makes-plug-and-backup-different)
    - [Screen casts](#screen-casts)
        - [Setting Up A New Backup](#setting-up-a-new-backup)
        - [Running A Backup And Restore](#running-a-backup-and-restore)
        - [Manage Backups](#manage-backups)
    - [Code Statistics](#code-statistics)
- [About The Author](#about-the-author)
- [Impressum, Legal Note](#impressum-legal-note)

<!-- /TOC -->


After several weeks of work on my weekends, my new Windows Tool *Plug-And-Backup* is released
to the public.

The idea for this tiny backup solution came as my daughter lost all her pictures from her notebook
a few months ago. I was looking for a really simple and easy to use backup tool for really 
lazy people.

So I searched for a tool that was able to start a backup without any user interaction just by
plugging a USB thumb drive (or something similar) into the computer.

As the whole internet seems to be flooded with Windows backup tools, I wasn't able to find one
that matched my criteria *and* was free and open sourced.

So the idea developed into the plan to write my own backup tool. *Plug-And-Backup* was born.

## What makes Plug-And-Backup different?

- Maximum ease of use: Just plug your backup device into your PC to make a backup. That's all.
- Standard technologies, no proprietary file formats: *Plug-And-Backup* generates 7-Zip files
which can be restored on quite any device without installing *Plug-And-Backup* itself.
- Don't think about a backup strategy. If *Plug-And-Backup* doesn't find a former backup, it makes
a full backup. If it does, it makes a differential one.
- Reliability: *Plug-And-Backup* creates log files, which can automatically be sent via email.
No SMTP server or complicated settings are required (thanks to [Mailgun](https://mailgun.com)).
If something went wrong, you'll immediately know about it.
- Easy to use GUI interface for creating new backup sets. Let your device be detected, drag 
and drop one or more folders to the list, save & go.
- A simple recovery tool tool (along with a copy of the 7-Zip portable version) will be copied
to every backup device. You don'St need more than the backup device in your pocket to recover 
your data.
- Secure your backups with a password (optional).

## Screen casts

### Setting Up A New Backup

<video width="480" height="320" controls="controls">
<source src="https://plug-and-backup.eu/screencasts/Setting_up_a_new_backup.mp4" type="video/mp4">
</video>

<!-- you *must* offer a download link as they may be able to play the file locally. customise this bit all you want -->
<p> <strong>Download Video:</strong>
    MP4 Format:  <a href="https://plug-and-backup.eu/screencasts/Setting_up_a_new_backup.mp4">"MP4"</a>
    GIF Format:  <a href="https://plug-and-backup.eu/screencasts/Setting_up_a_new_backup.gif">"GIF"</a>
</p>

### Running A Backup And Restore

<video width="480" height="320" controls="controls">
<source src="https://plug-and-backup.eu/screencasts/Running_a_backup_and_Restore.mp4" type="video/mp4">
</video>

<!-- you *must* offer a download link as they may be able to play the file locally. customise this bit all you want -->
<p> <strong>Download Video:</strong>
    MP4 Format:  <a href="https://plug-and-backup.eu/screencasts/Running_a_backup_and_Restore.mp4">"MP4"</a>
    GIF Format:  <a href="https://plug-and-backup.eu/screencasts/Running_a_backup_and_Restore.gif">"GIF"</a>
</p>


### Manage Backups

<video width="480" height="320" controls="controls">
<source src="https://plug-and-backup.eu/screencasts/Backup_manager.mp4" type="video/mp4">
</video>

<!-- you *must* offer a download link as they may be able to play the file locally. customise this bit all you want -->
<p> <strong>Download Video:</strong>
    MP4 Format:  <a href="https://plug-and-backup.eu/screencasts/Backup_manager.mp4">"MP4"</a>
    GIF Format:  <a href="https://plug-and-backup.eu/screencasts/Backup_manager.gif">"GIF"</a>
</p>

## Code Statistics

Project's source code statistics as of version 1.6.0:

cloc|github.com/AlDanial/cloc v 1.72  T=3.00 s (44.0 files/s, 82552.7 lines/s)
--- | ---

Language|files|blank|comment|code
:-------|-------:|-------:|-------:|-------:
XML|30|670|242|233456
Visual Basic|44|447|756|3318
XSD|1|0|0|2978
HTML|17|460|247|2655
MSBuild script|4|0|21|672
CSS|7|29|32|613
Markdown|10|180|0|450
JavaScript|11|58|36|159
INI|3|17|0|77
Razor|2|4|2|32
PHP|1|5|13|27
DOS Batch|2|0|0|2
--------|--------|--------|--------|--------
SUM:|132|1870|1349|244439

# About The Author

My name is Markus Busche, living in Kiel, Germany.

I'm addicted to free and open source software. Beside my job as system administrator I programmed 
some - mostly for myself - useful tools and published a handful of them as open source software.

An (incomplete) list of my works:
  - Film//Riss (inactive)
  - Netio-GUI (stable)
  - VPN-Connect (stable)


My latest project is called *Plug-And-Backup*.

I'm not the best programmer, but it has been a hobby since the late 1990ies and I'm always interested
in the latest technologies.

This blog is made with Wyam. I just wanted to use something other than wordpress and the other monster 
frameworks for making a simple static site that's still nice and clean. So I will give it a try.



For legal reasons:

---
# Impressum, Legal Note

Angaben gemäß § 5 Telemediengesetz (TMG):

Markus Busche, Knorrstr. 16, 24106 Kiel, Deutschland

E-Mail: [m.busche@gmail.com](mailto://m.busche@gmail.com)
