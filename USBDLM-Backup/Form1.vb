﻿Imports System.Globalization
Imports System.IO
Imports MadMilkman.Ini

Public Class Form1
    Public sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    Public sAppDataPath As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Plug-And-Backup"
    Dim sPaBINIFile As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup\PaB.ini"
    Public iStep As Integer = 0
    Dim ci As CultureInfo = CultureInfo.InstalledUICulture
    Dim sLanguage As String = ci.Name
    Dim sDeviceID As String = Nothing
    Dim olddevices, newdevices As List(Of String)

    ''' <summary>
    ''' Cancel (resets Form1)
    ''' </summary>
    Private Sub _CancelProcess()
        If sLanguage = "de-DE" Then
            MsgBox("Kein Gerät erkannt, Vorgang abgebrochen.", MsgBoxStyle.Information)
        Else
            MsgBox("No device detected, process cancelled.", MsgBoxStyle.Information)
        End If
        _Startinstructions()
    End Sub

    ''' <summary>
    ''' Set some controls enabled / disabled
    ''' </summary>
    ''' <param name="bEnabled"></param>
    Private Sub _SetControlsEnabled(ByVal bEnabled As Boolean)
        ' Hide Controls
        txtPath.Enabled = bEnabled
        btnSelectPath.Enabled = bEnabled
        cboxUsePassword.Enabled = bEnabled
        If bEnabled = False Then
            txtPassword.Enabled = bEnabled
        End If
        txtDescription.Enabled = bEnabled
        liviDirectories.Enabled = bEnabled
        liviDirectories.AllowDrop = bEnabled
        btnAddDirectory.Enabled = bEnabled
    End Sub

    ''' <summary>
    ''' Reset intructions and controls to the beginning
    ''' </summary>
    Private Sub _Startinstructions()
        liviDirectories.Items.Clear()
        txtDescription.Text = ""
        txtPassword.Text = Nothing
        txtPassword.Enabled = False
        cboxUsePassword.Checked = False
        iStep = 0
        btnCancel.Enabled = False
        If sLanguage = "de-DE" Then
            lblInstructions.Text = "Klicke auf den Button unten, um " + vbCrLf +
                                   "eine automatische Datensicherung" + vbCrLf +
                                   "auf einem Wechseldatenträger" + vbCrLf +
                                   "einzurichten."
            btnSetup.Text = "&Neuen Backupdatenträger einrichten"
        Else
            lblInstructions.Text = "Click on the button below to setup" + vbCrLf +
                                   "an automated backup on a removable" + vbCrLf +
                                   "device."
            btnSetup.Text = "&Setup New backup device"
        End If
    End Sub


    ''' <summary>
    ''' Multistep wizard for setting up a new backup device
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSetup_Click(sender As Object, e As EventArgs) Handles btnSetup.Click

        If iStep = 4 And liviDirectories.Items.Count = 0 Then
            If sLanguage = "de-DE" Then
                MsgBox("Du hast noch keine Verzeichnisse zur Sicherung ausgewählt.", MsgBoxStyle.Information)
            Else
                MsgBox("You haven´t selected any directories to be backed up, yet.", MsgBoxStyle.Information)
            End If
            Exit Sub
        End If

        iStep += 1


        Select Case iStep
            Case 1
                If sLanguage = "de-DE" Then
                    lblInstructions.Text = "Falls der Wechseldatenträger ange-" + vbCrLf +
                                   "schlossen ist, entferne ihn jetzt." + vbCrLf +
                                   "Klicke anschließend auf 'Weiter'."
                    btnSetup.Text = "&Weiter"
                Else
                    lblInstructions.Text = "Remove the backup device now if it" + vbCrLf +
                                   "is plugged in. When done click 'Next'."
                    btnSetup.Text = "&Next"
                End If

            Case 2
                btnCancel.Enabled = True
                TopMost = True
                ' Get USB volumes without backup device
                olddevices = getvolumes()
                ' Get installed volumes with backup device plugged in
                If sLanguage = "de-DE" Then
                    lblInstructions.Text = "Schließe jetzt den Backupdaten-" + vbCrLf +
                                           "träger an. Warte einen Moment, bis" + vbCrLf +
                                           "Windows ihn erkannt hat. Klicke an-" + vbCrLf +
                                           "schließend auf 'Weiter'."
                Else
                    lblInstructions.Text = "Now, plug in your backup device" + vbCrLf +
                                           "and wait a moment until it´s regis-" + vbCrLf +
                                           "tered. Then click 'Next'."
                End If

            Case 3
                newdevices = getvolumes()
                ' Compare new devices with old devices to find the ID of the new device
                Dim Duplicates As New List(Of String)
                For Each sDevN In newdevices
                    For Each sDevO In olddevices
                        If sDevN = sDevO Then
                            Duplicates.Add(sDevN)
                        End If
                    Next
                Next
                For Each sDupe In Duplicates
                    newdevices.Remove(sDupe)
                Next

                If newdevices.Count > 0 Then
                    sDeviceID = newdevices(0)
                End If

                If sDeviceID Is Nothing Then
                    _CancelProcess()
                    Exit Sub
                End If

                Dim dev_exists_in_ini As Boolean = False
                Dim IniFile As IniFile = New IniFile()
                If File.Exists(sPaBINIFile) Then
                    IniFile.Load(sPaBINIFile)

                    Dim section As IniSection = Nothing
                    Try
                        For Each sect In IniFile.Sections()
                            If sect.Name.Contains("Device_" + sDeviceID.Split("\")(2)) Then
                                dev_exists_in_ini = True
                                section = sect
                                Exit For
                            End If
                        Next
                    Catch
                    End Try
                    If dev_exists_in_ini Then
                        Dim result As DialogResult = Nothing
                        If sLanguage = "de-DE" Then
                            result = MsgBox("Es existiert bereits ein Backupset für diesen Datenträger. Das Set wird überschrieben!",
                               MsgBoxStyle.OkCancel)
                        Else
                            result = MsgBox("A backup set for this device already exists. It will be overwritten now.", MsgBoxStyle.OkCancel)
                        End If
                        If result = DialogResult.OK Then
                            Try
                                IniFile.Sections.Remove(section)
                                IniFile.Save(sPaBINIFile)
                            Catch ex As Exception
                                ' TODO: Fehlermeldung
                                _CancelProcess()
                            End Try
                        Else
                            _CancelProcess()
                        End If
                    End If
                End If

                If sLanguage = "de-DE" Then
                    lblInstructions.Text = "Dieses Backup-Gerät hat die ID" + vbCrLf +
                                           sDeviceID.Split("\")(2) + vbCrLf + vbCrLf +
                                           "Klicke 'Weiter' um fortzufahren."
                Else
                    lblInstructions.Text = "The ID of this backup device is" + vbCrLf +
                                           sDeviceID.Split("\")(2) + vbCrLf + vbCrLf +
                                           "Click 'Next' to continue."
                End If
                TopMost = False
            Case 4
                ' Show controls
                _SetControlsEnabled(True)

                If sLanguage = "de-DE" Then
                    lblInstructions.Text = "Füge nun Verzeichnisse, die gesich-" + vbCrLf +
                                           "ert werden sollen, hinzu." + vbCrLf +
                                           "Klicke anschließend auf 'Aktivieren'." + vbCrLf +
                                           "Verzeichnisse können auch per Drag" + vbCrLf +
                                           "and Drop hinzugefügt werden."
                    btnSetup.Text = "A&ktivieren"
                Else
                    lblInstructions.Text = "Now, add folder(s) to be backed up" + vbCrLf +
                                           "onto this device." + vbCrLf +
                                           "Then, click 'Activate'."
                    btnSetup.Text = "A&ctivate"
                End If
            Case 5
                btnCancel.Enabled = False
                _SetControlsEnabled(False)
                _checkServiceState()
                If liviDirectories.Items.Count > 0 Then
                    ' Awesome oneliner from https://stackoverflow.com/questions/2673640/getting-listview-values-into-a-string-array
                    Dim sDirectories() = liviDirectories.Items.Cast(Of ListViewItem).Select(Function(lvi As ListViewItem) lvi.SubItems(0).Text).ToArray()
                    ' Remove duplicates (https://stackoverflow.com/questions/9673/remove-duplicates-from-array)
                    sDirectories = sDirectories.Distinct().ToArray()

                    Dim sDescription As String = txtDescription.Text
                    If sDescription = "" Then
                        sDescription = "PaB Backup " + DateTime.Now.ToString
                    End If

                    If txtPassword.Text <> Nothing And cboxUsePassword.Checked = True Then
                        WriteIni(sDeviceID, sDirectories, sDescription, txtPassword.Text)
                    Else
                        WriteIni(sDeviceID, sDirectories, sDescription)
                    End If
                Else
                    If sLanguage = "de-DE" Then
                        MsgBox("Füge mindestens ein Verzeichnis zur Auswahl hinzu.", MsgBoxStyle.Information)
                        iStep -= 1
                        Exit Sub
                    Else
                        MsgBox("Add at least one directory before you proceed.", MsgBoxStyle.Information)
                        iStep -= 1
                        Exit Sub
                    End If
                End If

                If sLanguage = "de-DE" Then
                    lblInstructions.Text = "Die Sicherung ist fertig einge-" + vbCrLf +
                                           "richtet. Entferne und stecke den" + vbCrLf +
                                           "Datenträger, um das Backup zu" + vbCrLf +
                                           "testen."
                    btnSetup.Text = "&Zurück zum Anfang"
                Else
                    lblInstructions.Text = "The setup is now complete." + vbCrLf +
                                           "Unplug and plug the device to" + vbCrLf +
                                           "start your backup."
                    btnSetup.Text = "&Back to the start"
                End If

            Case 6
                _Startinstructions()
        End Select
    End Sub

    Private Sub _checkServiceState()
        If WindowsServiceControl.IsServiceRunning("PlugAndBackupService") Then
            ' Everything looks fine
            Exit Sub
        Else
            If WindowsServiceControl.IsServiceInstalled("PlugAndBackupService") Then
                ' Service installed but not running
                If sLanguage = "de-DE" Then
                    MsgBox("Der Plug-And-Backup Dienst ist nicht gestartet. Ohne laufenden Dienst kann kein Backup gestartet werden. " +
                           "Starte den Dienst über das Tools Menü.", MsgBoxStyle.Information)
                Else
                    MsgBox("The Plug-And-Backup service is not running. Without it, no backup can be started. Start the service with " +
                           "the Tools menu.", MsgBoxStyle.Information)
                End If
            Else
                ' Service is not installed
                If sLanguage = "de-DE" Then
                    MsgBox("Der Plug-And-Backup Dienst ist nicht installiert.  Ohne installierten Dienst kann kein Backup gestartet werden. " +
                           "Installiere und starte den Dienst über das Tools Menü.", MsgBoxStyle.Information)
                Else
                    MsgBox("The Plug-And-Backup service is not installed. Without it, no backup can be started. Install and start the service " +
                           "with the Tools menu.", MsgBoxStyle.Information)
                End If
            End If
        End If
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Set window position
        Location = New Point(My.Settings.WindowPosX, My.Settings.WindowPosY)
        _SetControlsEnabled(False)

        If sLanguage = "de-DE" Then
            txtPath.Text = "Ordner hier auswählen -->"
        Else
            txtPath.Text = "Select folder here -->"
        End If
        _Startinstructions()
    End Sub

    ''' <summary>
    ''' OpenFileDialog as path selection (don´t like FolderBrowserDialog)
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSelectPath_Click(sender As Object, e As EventArgs) Handles btnSelectPath.Click
        Dim ofd As OpenFileDialog = New OpenFileDialog
        With ofd
            .CheckPathExists = True
            .CheckFileExists = False
            .FileName = "ignore.me"
            .InitialDirectory = Environment.SpecialFolder.MyComputer
        End With
        Dim result As DialogResult = ofd.ShowDialog
        If result = DialogResult.OK Then
            txtPath.Text = Path.GetDirectoryName(ofd.FileName)
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub btnAddDirectory_Click(sender As Object, e As EventArgs) Handles btnAddDirectory.Click
        liviDirectories.ShowItemToolTips = True
        If Directory.Exists(txtPath.Text) Then
            Dim item As ListViewItem = New ListViewItem
            item.Text = txtPath.Text
            item.ToolTipText = item.Text
            liviDirectories.Items.Add(item)
            txtPath.Text = Nothing
        Else
            If sLanguage = "de-DE" Then
                MsgBox("Das ausgewählte Verzeichnis exitiert nicht.", MsgBoxStyle.Information)
            Else
                MsgBox("The selected directory doesn´t exist", MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub liviDirectories_DragDrop(sender As Object, e As DragEventArgs) Handles liviDirectories.DragDrop
        Dim files() As String = e.Data.GetData(DataFormats.FileDrop, False)
        For Each sPath In files
            If File.Exists(sPath) Then
                sPath = Path.GetDirectoryName(sPath)
            End If

            If Directory.Exists(sPath) Then
                Dim item As ListViewItem = New ListViewItem
                item.Text = sPath
                item.ToolTipText = item.Text
                liviDirectories.Items.Add(item)
                txtPath.Text = Nothing
            End If
        Next
    End Sub

    Private Sub cboxUsePassword_CheckedChanged_1(sender As Object, e As EventArgs) Handles cboxUsePassword.CheckedChanged
        txtPassword.Enabled = cboxUsePassword.Checked
    End Sub

    Private Sub Start7ZipFilemanagerToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles Start7ZipFilemanagerToolStripMenuItem.Click
        Try
            Process.Start(sBasepath + "\7-ZipPortable\7-ZipPortable.exe")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub RdPartyLicensesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RdPartyLicensesToolStripMenuItem.Click
        Dim tm = TopMost
        TopMost = False
        Licenses.ShowDialog()
        TopMost = tm
    End Sub

    Private Sub ManageBackupSetsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManageBackupSetsToolStripMenuItem.Click
        ManageBackups.ShowDialog()
    End Sub

    Private Sub RemoveEntryToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RemoveEntryToolStripMenuItem.Click
        liviDirectories.Items.RemoveAt(liviDirectories.SelectedIndices(0))
    End Sub

    ' Save window position
    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.WindowPosX = Location.X
        My.Settings.WindowPosY = Location.Y
    End Sub

    Private Sub RegisterUSBDLMServiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RegisterUSBDLMServiceToolStripMenuItem.Click
        Dim regservice As Integer = ServiceControl._register()
        Dim startservice As Integer = ServiceControl._start
        If regservice = 0 Then
            If startservice = 0 Then
                If sLanguage = "de-DE" Then
                    MsgBox("Dienst erfolgreich registriert und gestartet.", MsgBoxStyle.Information)
                Else
                    MsgBox("Service registered and started successfully.", MsgBoxStyle.Information)
                End If
            End If
        ElseIf regservice = 1056 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der PaB-Dienst ist bereits installiert und läuft.", MsgBoxStyle.Information)
            Else
                MsgBox("PaB service is installed and running yet.", MsgBoxStyle.Information)
            End If
        Else
            If sLanguage = "de-DE" Then
                MsgBox("Fehler beim registrieren des Dienstes: " + regservice.ToString, MsgBoxStyle.Critical)
            Else
                MsgBox("Regsitering service failed: " + regservice.ToString, MsgBoxStyle.Critical)
            End If
        End If

    End Sub

    Private Sub UnregisterUSBDLMServiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles UnregisterUSBDLMServiceToolStripMenuItem.Click
        Dim stopservice = ServiceControl._stop()
        Dim unregservice = ServiceControl._deregister()

        If unregservice = 0 Then
            If sLanguage = "de-DE" Then
                MsgBox("Dienst erfolgreich beendet und deregistriert.", MsgBoxStyle.Information)
            Else
                MsgBox("Service successfully stopped and unregistered.", MsgBoxStyle.Information)
            End If
        ElseIf unregservice = 1060 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der Dienst ist nicht registriert.", MsgBoxStyle.Information)
            Else
                MsgBox("The service is not registered.", MsgBoxStyle.Information)
            End If
        Else
            If sLanguage = "de-DE" Then
                MsgBox("Fehler beim deregistrieren des Dienstes: " + unregservice.ToString, MsgBoxStyle.Critical)
            Else
                MsgBox("Deregsitering service failed: " + unregservice.ToString, MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub StartUSBDLMServiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StartUSBDLMServiceToolStripMenuItem.Click
        Dim servicestart As Integer = ServiceControl._start()
        If servicestart = 0 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der Dienst wurde gestartet.", MsgBoxStyle.Information)
            Else
                MsgBox("Service started successfully.", MsgBoxStyle.Information)
            End If
        ElseIf servicestart = 1056 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der Dienst läuft bereits.", MsgBoxStyle.Information)
            Else
                MsgBox("Service is running yet.", MsgBoxStyle.Information)
            End If
        Else
            If sLanguage = "de-DE" Then
                MsgBox("Fehler beim Starten des Dienstes: " + servicestart.ToString, MsgBoxStyle.Critical)
            Else
                MsgBox("Starting the service failed: " + servicestart.ToString, MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub StoptUSBDLMServiceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles StoptUSBDLMServiceToolStripMenuItem.Click
        Dim servicestop As Integer = ServiceControl._stop()
        If servicestop = 0 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der Dienst wurde beendet.", MsgBoxStyle.Information)
            Else
                MsgBox("Service successfully halted.", MsgBoxStyle.Information)
            End If
        ElseIf servicestop = 1062 Then
            If sLanguage = "de-DE" Then
                MsgBox("Der Dienst konnte nicht beendet werden, da er nicht gestartet ist.", MsgBoxStyle.Information)
            Else
                MsgBox("Could not stop the service because it isn't running.", MsgBoxStyle.Information)
            End If
        Else

            If sLanguage = "de-DE" Then
                MsgBox("Fehler beim Beenden des Dienstes: " + servicestop.ToString, MsgBoxStyle.Critical)
            Else
                MsgBox("Stopping the service failed: " + servicestop.ToString, MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub liviDirectories_DragEnter(sender As Object, e As DragEventArgs) Handles liviDirectories.DragEnter
        If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
            e.Effect = DragDropEffects.Copy
        Else
            e.Effect = DragDropEffects.None
        End If
    End Sub

    Private Sub ManuallyeditUSBDLMINIToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ManuallyeditUSBDLMINIToolStripMenuItem.Click
        If File.Exists(sPaBINIFile) Then
            Process.Start("notepad.exe", sPaBINIFile)
        Else
            If sLanguage = "de-DE" Then
                MsgBox("Datei existiert (noch) nicht: " + sPaBINIFile, MsgBoxStyle.Information)
            Else
                MsgBox("File does not exist (yet): " + sPaBINIFile, MsgBoxStyle.Information)
            End If
        End If
    End Sub

    Private Sub CheckForupdateToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CheckForupdateToolStripMenuItem.Click
        Dim prGup As Process = New Process
        Dim sArgs As String = Str(Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major) + "." +
                Str(Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor) + "." +
                Str(Reflection.Assembly.GetExecutingAssembly().GetName().Version.Revision)
        sArgs = "-v " + sArgs.Replace(" ", "")
        With prGup.StartInfo
            .FileName = sBasepath + "\wingup\gup.exe"
            .WorkingDirectory = sBasepath + "\wingup"
            .Arguments = sArgs
        End With
        Try
            prGup.Start()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ReportABugToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles ReportABugToolStripMenuItem1.Click
        Process.Start("https://gitlab.com/m-busche/Plug-And-Backup/issues")
    End Sub

    Private Sub ProjectHomepageToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ProjectHomepageToolStripMenuItem.Click
        Process.Start("https://plug-and-backup.eu/")
    End Sub

    Private Sub DocumentationToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DocumentationToolStripMenuItem.Click
        Process.Start("https://plug-and-backup.eu/documentation")
    End Sub

    Private Sub ShowLogFilesToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles ShowLogFilesToolStripMenuItem.Click
        Process.Start("explorer.exe", Path.GetDirectoryName(sPaBINIFile) + "\logs")
    End Sub

    Private Sub ChangelogToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ChangelogToolStripMenuItem.Click
        Process.Start("https://plug-and-backup.eu/download")
    End Sub


    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        _Startinstructions()
    End Sub
End Class
