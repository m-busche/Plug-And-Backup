﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManageBackupSetsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShowLogFilesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegisterUSBDLMServiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UnregisterUSBDLMServiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StartUSBDLMServiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StoptUSBDLMServiceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ManuallyeditUSBDLMINIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Start7ZipFilemanagerToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RdPartyLicensesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckForupdateToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportABugToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProjectHomepageToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportABugToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DocumentationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ChangelogToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSetup = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDescription = New System.Windows.Forms.TextBox()
        Me.liviDirectories = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.RemoveEntryToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.cboxUsePassword = New System.Windows.Forms.CheckBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.btnAddDirectory = New System.Windows.Forms.Button()
        Me.btnSelectPath = New System.Windows.Forms.Button()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.lblInstructions = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ToolsToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.ToolTip1.SetToolTip(Me.MenuStrip1, resources.GetString("MenuStrip1.ToolTip"))
        '
        'FileToolStripMenuItem
        '
        resources.ApplyResources(Me.FileToolStripMenuItem, "FileToolStripMenuItem")
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ManageBackupSetsToolStripMenuItem, Me.ShowLogFilesToolStripMenuItem, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        '
        'ManageBackupSetsToolStripMenuItem
        '
        resources.ApplyResources(Me.ManageBackupSetsToolStripMenuItem, "ManageBackupSetsToolStripMenuItem")
        Me.ManageBackupSetsToolStripMenuItem.Name = "ManageBackupSetsToolStripMenuItem"
        '
        'ShowLogFilesToolStripMenuItem
        '
        resources.ApplyResources(Me.ShowLogFilesToolStripMenuItem, "ShowLogFilesToolStripMenuItem")
        Me.ShowLogFilesToolStripMenuItem.Name = "ShowLogFilesToolStripMenuItem"
        '
        'ExitToolStripMenuItem
        '
        resources.ApplyResources(Me.ExitToolStripMenuItem, "ExitToolStripMenuItem")
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        '
        'ToolsToolStripMenuItem
        '
        resources.ApplyResources(Me.ToolsToolStripMenuItem, "ToolsToolStripMenuItem")
        Me.ToolsToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegisterUSBDLMServiceToolStripMenuItem, Me.UnregisterUSBDLMServiceToolStripMenuItem, Me.StartUSBDLMServiceToolStripMenuItem, Me.StoptUSBDLMServiceToolStripMenuItem, Me.ManuallyeditUSBDLMINIToolStripMenuItem, Me.Start7ZipFilemanagerToolStripMenuItem})
        Me.ToolsToolStripMenuItem.Name = "ToolsToolStripMenuItem"
        '
        'RegisterUSBDLMServiceToolStripMenuItem
        '
        resources.ApplyResources(Me.RegisterUSBDLMServiceToolStripMenuItem, "RegisterUSBDLMServiceToolStripMenuItem")
        Me.RegisterUSBDLMServiceToolStripMenuItem.Name = "RegisterUSBDLMServiceToolStripMenuItem"
        '
        'UnregisterUSBDLMServiceToolStripMenuItem
        '
        resources.ApplyResources(Me.UnregisterUSBDLMServiceToolStripMenuItem, "UnregisterUSBDLMServiceToolStripMenuItem")
        Me.UnregisterUSBDLMServiceToolStripMenuItem.Name = "UnregisterUSBDLMServiceToolStripMenuItem"
        '
        'StartUSBDLMServiceToolStripMenuItem
        '
        resources.ApplyResources(Me.StartUSBDLMServiceToolStripMenuItem, "StartUSBDLMServiceToolStripMenuItem")
        Me.StartUSBDLMServiceToolStripMenuItem.Name = "StartUSBDLMServiceToolStripMenuItem"
        '
        'StoptUSBDLMServiceToolStripMenuItem
        '
        resources.ApplyResources(Me.StoptUSBDLMServiceToolStripMenuItem, "StoptUSBDLMServiceToolStripMenuItem")
        Me.StoptUSBDLMServiceToolStripMenuItem.Name = "StoptUSBDLMServiceToolStripMenuItem"
        '
        'ManuallyeditUSBDLMINIToolStripMenuItem
        '
        resources.ApplyResources(Me.ManuallyeditUSBDLMINIToolStripMenuItem, "ManuallyeditUSBDLMINIToolStripMenuItem")
        Me.ManuallyeditUSBDLMINIToolStripMenuItem.Name = "ManuallyeditUSBDLMINIToolStripMenuItem"
        '
        'Start7ZipFilemanagerToolStripMenuItem
        '
        resources.ApplyResources(Me.Start7ZipFilemanagerToolStripMenuItem, "Start7ZipFilemanagerToolStripMenuItem")
        Me.Start7ZipFilemanagerToolStripMenuItem.Name = "Start7ZipFilemanagerToolStripMenuItem"
        '
        'HelpToolStripMenuItem
        '
        resources.ApplyResources(Me.HelpToolStripMenuItem, "HelpToolStripMenuItem")
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RdPartyLicensesToolStripMenuItem, Me.CheckForupdateToolStripMenuItem, Me.ReportABugToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        '
        'RdPartyLicensesToolStripMenuItem
        '
        resources.ApplyResources(Me.RdPartyLicensesToolStripMenuItem, "RdPartyLicensesToolStripMenuItem")
        Me.RdPartyLicensesToolStripMenuItem.Name = "RdPartyLicensesToolStripMenuItem"
        '
        'CheckForupdateToolStripMenuItem
        '
        resources.ApplyResources(Me.CheckForupdateToolStripMenuItem, "CheckForupdateToolStripMenuItem")
        Me.CheckForupdateToolStripMenuItem.Name = "CheckForupdateToolStripMenuItem"
        '
        'ReportABugToolStripMenuItem
        '
        resources.ApplyResources(Me.ReportABugToolStripMenuItem, "ReportABugToolStripMenuItem")
        Me.ReportABugToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProjectHomepageToolStripMenuItem, Me.ReportABugToolStripMenuItem1, Me.DocumentationToolStripMenuItem, Me.ChangelogToolStripMenuItem})
        Me.ReportABugToolStripMenuItem.Name = "ReportABugToolStripMenuItem"
        '
        'ProjectHomepageToolStripMenuItem
        '
        resources.ApplyResources(Me.ProjectHomepageToolStripMenuItem, "ProjectHomepageToolStripMenuItem")
        Me.ProjectHomepageToolStripMenuItem.Name = "ProjectHomepageToolStripMenuItem"
        '
        'ReportABugToolStripMenuItem1
        '
        resources.ApplyResources(Me.ReportABugToolStripMenuItem1, "ReportABugToolStripMenuItem1")
        Me.ReportABugToolStripMenuItem1.Name = "ReportABugToolStripMenuItem1"
        '
        'DocumentationToolStripMenuItem
        '
        resources.ApplyResources(Me.DocumentationToolStripMenuItem, "DocumentationToolStripMenuItem")
        Me.DocumentationToolStripMenuItem.Name = "DocumentationToolStripMenuItem"
        '
        'ChangelogToolStripMenuItem
        '
        resources.ApplyResources(Me.ChangelogToolStripMenuItem, "ChangelogToolStripMenuItem")
        Me.ChangelogToolStripMenuItem.Name = "ChangelogToolStripMenuItem"
        '
        'btnSetup
        '
        resources.ApplyResources(Me.btnSetup, "btnSetup")
        Me.btnSetup.Name = "btnSetup"
        Me.ToolTip1.SetToolTip(Me.btnSetup, resources.GetString("btnSetup.ToolTip"))
        Me.btnSetup.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtDescription)
        Me.GroupBox1.Controls.Add(Me.liviDirectories)
        Me.GroupBox1.Controls.Add(Me.cboxUsePassword)
        Me.GroupBox1.Controls.Add(Me.txtPassword)
        Me.GroupBox1.Controls.Add(Me.btnAddDirectory)
        Me.GroupBox1.Controls.Add(Me.btnSelectPath)
        Me.GroupBox1.Controls.Add(Me.txtPath)
        Me.GroupBox1.Controls.Add(Me.lblInstructions)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.GroupBox1, resources.GetString("GroupBox1.ToolTip"))
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        Me.ToolTip1.SetToolTip(Me.Label1, resources.GetString("Label1.ToolTip"))
        '
        'txtDescription
        '
        resources.ApplyResources(Me.txtDescription, "txtDescription")
        Me.txtDescription.Name = "txtDescription"
        Me.ToolTip1.SetToolTip(Me.txtDescription, resources.GetString("txtDescription.ToolTip"))
        '
        'liviDirectories
        '
        resources.ApplyResources(Me.liviDirectories, "liviDirectories")
        Me.liviDirectories.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1})
        Me.liviDirectories.ContextMenuStrip = Me.ContextMenuStrip1
        Me.liviDirectories.MultiSelect = False
        Me.liviDirectories.Name = "liviDirectories"
        Me.ToolTip1.SetToolTip(Me.liviDirectories, resources.GetString("liviDirectories.ToolTip"))
        Me.liviDirectories.UseCompatibleStateImageBehavior = False
        Me.liviDirectories.View = System.Windows.Forms.View.List
        '
        'ColumnHeader1
        '
        resources.ApplyResources(Me.ColumnHeader1, "ColumnHeader1")
        '
        'ContextMenuStrip1
        '
        resources.ApplyResources(Me.ContextMenuStrip1, "ContextMenuStrip1")
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RemoveEntryToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ToolTip1.SetToolTip(Me.ContextMenuStrip1, resources.GetString("ContextMenuStrip1.ToolTip"))
        '
        'RemoveEntryToolStripMenuItem
        '
        resources.ApplyResources(Me.RemoveEntryToolStripMenuItem, "RemoveEntryToolStripMenuItem")
        Me.RemoveEntryToolStripMenuItem.Name = "RemoveEntryToolStripMenuItem"
        '
        'cboxUsePassword
        '
        resources.ApplyResources(Me.cboxUsePassword, "cboxUsePassword")
        Me.cboxUsePassword.Name = "cboxUsePassword"
        Me.ToolTip1.SetToolTip(Me.cboxUsePassword, resources.GetString("cboxUsePassword.ToolTip"))
        Me.cboxUsePassword.UseVisualStyleBackColor = True
        '
        'txtPassword
        '
        resources.ApplyResources(Me.txtPassword, "txtPassword")
        Me.txtPassword.Name = "txtPassword"
        Me.ToolTip1.SetToolTip(Me.txtPassword, resources.GetString("txtPassword.ToolTip"))
        '
        'btnAddDirectory
        '
        resources.ApplyResources(Me.btnAddDirectory, "btnAddDirectory")
        Me.btnAddDirectory.Name = "btnAddDirectory"
        Me.ToolTip1.SetToolTip(Me.btnAddDirectory, resources.GetString("btnAddDirectory.ToolTip"))
        Me.btnAddDirectory.UseVisualStyleBackColor = True
        '
        'btnSelectPath
        '
        resources.ApplyResources(Me.btnSelectPath, "btnSelectPath")
        Me.btnSelectPath.Name = "btnSelectPath"
        Me.ToolTip1.SetToolTip(Me.btnSelectPath, resources.GetString("btnSelectPath.ToolTip"))
        Me.btnSelectPath.UseVisualStyleBackColor = True
        '
        'txtPath
        '
        resources.ApplyResources(Me.txtPath, "txtPath")
        Me.txtPath.Name = "txtPath"
        Me.ToolTip1.SetToolTip(Me.txtPath, resources.GetString("txtPath.ToolTip"))
        '
        'lblInstructions
        '
        resources.ApplyResources(Me.lblInstructions, "lblInstructions")
        Me.lblInstructions.Name = "lblInstructions"
        Me.ToolTip1.SetToolTip(Me.lblInstructions, resources.GetString("lblInstructions.ToolTip"))
        '
        'btnCancel
        '
        resources.ApplyResources(Me.btnCancel, "btnCancel")
        Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnCancel.Name = "btnCancel"
        Me.ToolTip1.SetToolTip(Me.btnCancel, resources.GetString("btnCancel.ToolTip"))
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'NotifyIcon1
        '
        resources.ApplyResources(Me.NotifyIcon1, "NotifyIcon1")
        '
        'Form1
        '
        Me.AcceptButton = Me.btnSetup
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.btnCancel
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSetup)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.ToolTip1.SetToolTip(Me, resources.GetString("$this.ToolTip"))
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnSetup As Button
    Friend WithEvents ToolsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegisterUSBDLMServiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UnregisterUSBDLMServiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StartUSBDLMServiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StoptUSBDLMServiceToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents lblInstructions As Label
    Friend WithEvents btnSelectPath As Button
    Friend WithEvents txtPath As TextBox
    Friend WithEvents ManuallyeditUSBDLMINIToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RdPartyLicensesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents btnAddDirectory As Button
    Friend WithEvents cboxUsePassword As CheckBox
    Friend WithEvents txtPassword As TextBox
    Friend WithEvents Start7ZipFilemanagerToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents liviDirectories As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents btnCancel As Button
    Friend WithEvents ManageBackupSetsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents RemoveEntryToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents Label1 As Label
    Friend WithEvents txtDescription As TextBox
    Friend WithEvents NotifyIcon1 As NotifyIcon
    Friend WithEvents CheckForupdateToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportABugToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProjectHomepageToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportABugToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DocumentationToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ShowLogFilesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ChangelogToolStripMenuItem As ToolStripMenuItem
End Class
