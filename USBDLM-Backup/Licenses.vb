﻿Imports System.IO
Public Class Licenses
    Private Sub Licenses_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
        Dim sProductName As String = My.Application.Info.ProductName
        Dim sVersion As String = String.Format("Version {0}", My.Application.Info.Version.ToString)

        Dim abouttext As String =
            sProductName + " " + sVersion + vbCrLf + vbCrLf +
            "(c) 2017 Markus Busche (pab@butenostfreesen.de)" + vbCrLf +
            "Downloads, Documentation, News, Support: https://plug-and-backup.eu" + vbCrLf + vbCrLf +
            "This software is licensed under the MIT license (see tabs to the right)." + vbCrLf + vbCrLf +
            "If you want to donate my work, you're welcome:" + vbCrLf +
            "Paypal donation:  https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=7B4DEYJSA3TRS" + vbCrLf +
            "Flattr: https://flattr.com/submit/auto?fid=glgg90&url=https%3A%2F%2Fwww.heise.de%2Fdownload%2Fdeveloper%2Fmarkusbusche" + vbCrLf + vbCrLf +
            "Plug-And-Backup takes use of these other Open Source software libraries:" + vbCrLf + vbCrLf +
            "- 7-Zip (7-Zip license) [https://github.com/MarioZ/MadMilkman.Ini]" + vbCrLf +
            "- MadMilkMan.INI (Apache license 2.0) [https://github.com/MarioZ/MadMilkman.Ini]" + vbCrLf +
            "- Nlog (NLog license) [http://nlog-project.org/]" + vbCrLf +
            "- OpenIconCollection (OpenIconCollection) [https://sourceforge.net/projects/openiconlibrary/]" + vbCrLf +
            "- CreateProcessAsUser (MIT license) [https://github.com/murrayju/CreateProcessAsUser]" + vbCrLf +
            "- wingup (GPLv3 license) [https://github.com/gup4win/wingup]" + vbCrLf +
            "- RestSharp (Apache license 2.0) [https://github.com/restsharp/RestSharp]"

        rtbAbout.Text = abouttext
        rtbAutobackup.Rtf = My.Resources.MIT_License
        rtb7Zip.Rtf = My.Resources._7_Zip_License
        rtbIcons.Rtf = My.Resources.OpenIconLibrary_Licenses
        rtbApache.Rtf = My.Resources.Apache_License
        rtbNlog.Rtf = My.Resources.Nlog_License
        rtbGPLv3.Rtf = My.Resources.GPL3_License
    End Sub
End Class