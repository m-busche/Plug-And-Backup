﻿Imports System.ServiceProcess

Public Class WindowsServiceControl
    Public Shared Function IsServiceInstalled(serviceName As String) As Boolean
        ' get list of Windows services
        Dim services As ServiceController() = ServiceController.GetServices()

        ' try to find service name
        For Each service As ServiceController In services
            If service.ServiceName = serviceName Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Function IsServiceRunning(serviceName As String) As Boolean
        ' get list of Windows services
        Dim services As ServiceController() = ServiceController.GetServices()

        ' try to find service name
        For Each service As ServiceController In services
            If service.ServiceName = serviceName Then
                If service.Status = ServiceControllerStatus.Running Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function


    Public Shared Sub StartService(serviceName As String, timeoutMilliseconds As Integer)
        Dim service As New ServiceController(serviceName)
        Try
            Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)
            service.Start()
            service.WaitForStatus(ServiceControllerStatus.Running, timeout)
        Catch
        End Try
    End Sub

    Public Shared Sub StopService(serviceName As String, timeoutMilliseconds As Integer)
        Dim service As New ServiceController(serviceName)
        Try
            Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)
            service.[Stop]()
            service.WaitForStatus(ServiceControllerStatus.Stopped, timeout)
        Catch
        End Try
    End Sub

    Public Shared Sub RestartService(serviceName As String, timeoutMilliseconds As Integer)
        Dim service As New ServiceController(serviceName)
        Try
            Dim millisec1 As Integer = Environment.TickCount
            Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)

            service.[Stop]()
            service.WaitForStatus(ServiceControllerStatus.Stopped, timeout)

            ' count the rest of the timeout
            Dim millisec2 As Integer = Environment.TickCount
            timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1))

            service.Start()
            service.WaitForStatus(ServiceControllerStatus.Running, timeout)
        Catch
        End Try
    End Sub
End Class
