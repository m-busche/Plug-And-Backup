﻿Imports System.Net
Imports System.IO

Module DownloadFiles
    Dim sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    ''' <summary>
    ''' Download file from URL
    ''' </summary>
    ''' <returns>Filename if successful</returns>
    Private Function _Downloadfile(sURL, sFilename) As String
        Dim myUri As New Uri(sURL)
        Dim myWebClient As WebClient = New WebClient
        sFilename = Path.GetTempPath() + sFilename
        If File.Exists(sFilename) Then
            Try
                File.Delete(sFilename)
            Catch ex As Exception

            End Try
        End If
        Try
            myWebClient.DownloadFile(myUri, sFilename)
        Catch ex As Exception
            sFilename = Nothing
        End Try
        Return sFilename
    End Function

    ''' <summary>
    ''' Unzip downloaded zip file
    ''' </summary>
    ''' <param name="sFilename"></param>
    ''' <returns>False if fails</returns>
    Private Function _UnzipFile(sFilename) As Boolean
        Try
            Compression.ZipFile.ExtractToDirectory(sFilename, sBasepath + "\")
            _UnzipFile = True
        Catch ex As Exception
            _UnzipFile = False
        End Try
    End Function

    Public Function InstallFiles() As Boolean
        Dim sFilename As String = Nothing
        ' sFilename = _Downloadfile("http://www.uwe-sieber.de/files/usbdlm.zip", "usbdlm.zip")
        'Dim sFilename = _Downloadfile("http://www.uwe-sieber.de/files/usbdlm_beta.zip", "usbdlm.zip")
        'If Not sFilename = Nothing Then
        '    InstallFiles = _UnzipFile(sFilename)
        '    Try
        '        File.Delete(sFilename)
        '    Catch ex As Exception

        '    End Try
        '    If Not File.Exists(sBasepath + "\usbdlm\usbdlm.ini") Then
        '        File.Copy(sBasepath + "\PaB_usbdlm.ini", sBasepath + "\usbdlm\usbdlm.ini")
        '    End If

        'Else
        '        InstallFiles = False
        'End If
        'sFilename = _Downloadfile("http://www.uwe-sieber.de/files/removedrive.zip", "removedrive.zip")
        'If Not sFilename = Nothing Then
        '    InstallFiles = _UnzipFile(sFilename)
        '    Try
        '        File.Delete(sFilename)
        '        File.Move(sBasepath + "\win32\RemoveDrive.exe", sBasepath + "\usbdlm\RemoveDrive.exe")
        '        ' File.Move(sBasepath + "\win32\RemoveDrive.txt", sBasepath + "\usbdlm\RemoveDrive.txt")
        '        Directory.Delete(sBasepath + "\win32")
        '        Directory.Delete(sBasepath + "\x64", True)
        '    Catch ex As Exception

        '    End Try
        'Else
        '    InstallFiles = False
        'End If
        ' 7-Zip portable
        sFilename = _Downloadfile("https://nextcloud.butenostfreesen.de/index.php/s/6T8JNU1SR1Nu0uv/download", "7-Zip-Portable.zip")
        If Not sFilename = Nothing Then
            InstallFiles = _UnzipFile(sFilename)
            Try
                File.Delete(sFilename)
            Catch ex As Exception

            End Try
        Else
            InstallFiles = False
        End If
    End Function
End Module
