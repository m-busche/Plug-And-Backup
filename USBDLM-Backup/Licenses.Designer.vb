﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Licenses
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Licenses))
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.tpAbout = New System.Windows.Forms.TabPage()
        Me.rtbAbout = New System.Windows.Forms.TextBox()
        Me.tpMIT = New System.Windows.Forms.TabPage()
        Me.rtbAutobackup = New System.Windows.Forms.RichTextBox()
        Me.tpNLog = New System.Windows.Forms.TabPage()
        Me.rtbNlog = New System.Windows.Forms.RichTextBox()
        Me.tp7Zip = New System.Windows.Forms.TabPage()
        Me.rtb7Zip = New System.Windows.Forms.RichTextBox()
        Me.tpApache = New System.Windows.Forms.TabPage()
        Me.rtbApache = New System.Windows.Forms.RichTextBox()
        Me.tpOpenIcon = New System.Windows.Forms.TabPage()
        Me.rtbIcons = New System.Windows.Forms.RichTextBox()
        Me.tpGPLv3 = New System.Windows.Forms.TabPage()
        Me.rtbGPLv3 = New System.Windows.Forms.RichTextBox()
        Me.TabControl1.SuspendLayout()
        Me.tpAbout.SuspendLayout()
        Me.tpMIT.SuspendLayout()
        Me.tpNLog.SuspendLayout()
        Me.tp7Zip.SuspendLayout()
        Me.tpApache.SuspendLayout()
        Me.tpOpenIcon.SuspendLayout()
        Me.tpGPLv3.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TabControl1.Controls.Add(Me.tpAbout)
        Me.TabControl1.Controls.Add(Me.tpMIT)
        Me.TabControl1.Controls.Add(Me.tpNLog)
        Me.TabControl1.Controls.Add(Me.tp7Zip)
        Me.TabControl1.Controls.Add(Me.tpApache)
        Me.TabControl1.Controls.Add(Me.tpGPLv3)
        Me.TabControl1.Controls.Add(Me.tpOpenIcon)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Multiline = True
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(776, 599)
        Me.TabControl1.TabIndex = 0
        '
        'tpAbout
        '
        Me.tpAbout.Controls.Add(Me.rtbAbout)
        Me.tpAbout.Location = New System.Drawing.Point(4, 22)
        Me.tpAbout.Name = "tpAbout"
        Me.tpAbout.Padding = New System.Windows.Forms.Padding(3)
        Me.tpAbout.Size = New System.Drawing.Size(768, 573)
        Me.tpAbout.TabIndex = 4
        Me.tpAbout.Text = "About"
        Me.tpAbout.UseVisualStyleBackColor = True
        '
        'rtbAbout
        '
        Me.rtbAbout.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbAbout.Location = New System.Drawing.Point(7, 7)
        Me.rtbAbout.Multiline = True
        Me.rtbAbout.Name = "rtbAbout"
        Me.rtbAbout.ReadOnly = True
        Me.rtbAbout.Size = New System.Drawing.Size(755, 560)
        Me.rtbAbout.TabIndex = 0
        '
        'tpMIT
        '
        Me.tpMIT.Controls.Add(Me.rtbAutobackup)
        Me.tpMIT.Location = New System.Drawing.Point(4, 22)
        Me.tpMIT.Name = "tpMIT"
        Me.tpMIT.Padding = New System.Windows.Forms.Padding(3)
        Me.tpMIT.Size = New System.Drawing.Size(768, 573)
        Me.tpMIT.TabIndex = 0
        Me.tpMIT.Text = "MIT License"
        Me.tpMIT.UseVisualStyleBackColor = True
        '
        'rtbAutobackup
        '
        Me.rtbAutobackup.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbAutobackup.Location = New System.Drawing.Point(7, 7)
        Me.rtbAutobackup.Name = "rtbAutobackup"
        Me.rtbAutobackup.ReadOnly = True
        Me.rtbAutobackup.Size = New System.Drawing.Size(755, 560)
        Me.rtbAutobackup.TabIndex = 0
        Me.rtbAutobackup.Text = ""
        '
        'tpNLog
        '
        Me.tpNLog.Controls.Add(Me.rtbNlog)
        Me.tpNLog.Location = New System.Drawing.Point(4, 22)
        Me.tpNLog.Name = "tpNLog"
        Me.tpNLog.Size = New System.Drawing.Size(768, 573)
        Me.tpNLog.TabIndex = 6
        Me.tpNLog.Text = "NLog License"
        Me.tpNLog.UseVisualStyleBackColor = True
        '
        'rtbNlog
        '
        Me.rtbNlog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbNlog.Location = New System.Drawing.Point(6, 6)
        Me.rtbNlog.Name = "rtbNlog"
        Me.rtbNlog.ReadOnly = True
        Me.rtbNlog.Size = New System.Drawing.Size(756, 561)
        Me.rtbNlog.TabIndex = 2
        Me.rtbNlog.Text = ""
        '
        'tp7Zip
        '
        Me.tp7Zip.Controls.Add(Me.rtb7Zip)
        Me.tp7Zip.Location = New System.Drawing.Point(4, 22)
        Me.tp7Zip.Name = "tp7Zip"
        Me.tp7Zip.Padding = New System.Windows.Forms.Padding(3)
        Me.tp7Zip.Size = New System.Drawing.Size(768, 573)
        Me.tp7Zip.TabIndex = 3
        Me.tp7Zip.Text = "7-Zip License"
        Me.tp7Zip.UseVisualStyleBackColor = True
        '
        'rtb7Zip
        '
        Me.rtb7Zip.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtb7Zip.Location = New System.Drawing.Point(6, 6)
        Me.rtb7Zip.Name = "rtb7Zip"
        Me.rtb7Zip.ReadOnly = True
        Me.rtb7Zip.Size = New System.Drawing.Size(756, 561)
        Me.rtb7Zip.TabIndex = 2
        Me.rtb7Zip.Text = ""
        '
        'tpApache
        '
        Me.tpApache.Controls.Add(Me.rtbApache)
        Me.tpApache.Location = New System.Drawing.Point(4, 22)
        Me.tpApache.Name = "tpApache"
        Me.tpApache.Size = New System.Drawing.Size(768, 573)
        Me.tpApache.TabIndex = 5
        Me.tpApache.Text = "Apache License"
        Me.tpApache.UseVisualStyleBackColor = True
        '
        'rtbApache
        '
        Me.rtbApache.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbApache.Location = New System.Drawing.Point(6, 6)
        Me.rtbApache.Name = "rtbApache"
        Me.rtbApache.ReadOnly = True
        Me.rtbApache.Size = New System.Drawing.Size(756, 561)
        Me.rtbApache.TabIndex = 2
        Me.rtbApache.Text = ""
        '
        'tpOpenIcon
        '
        Me.tpOpenIcon.Controls.Add(Me.rtbIcons)
        Me.tpOpenIcon.Location = New System.Drawing.Point(4, 22)
        Me.tpOpenIcon.Name = "tpOpenIcon"
        Me.tpOpenIcon.Padding = New System.Windows.Forms.Padding(3)
        Me.tpOpenIcon.Size = New System.Drawing.Size(768, 573)
        Me.tpOpenIcon.TabIndex = 2
        Me.tpOpenIcon.Text = "OpenIconCollection"
        Me.tpOpenIcon.UseVisualStyleBackColor = True
        '
        'rtbIcons
        '
        Me.rtbIcons.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbIcons.Location = New System.Drawing.Point(6, 6)
        Me.rtbIcons.Name = "rtbIcons"
        Me.rtbIcons.ReadOnly = True
        Me.rtbIcons.Size = New System.Drawing.Size(756, 561)
        Me.rtbIcons.TabIndex = 1
        Me.rtbIcons.Text = ""
        '
        'tpGPLv3
        '
        Me.tpGPLv3.Controls.Add(Me.rtbGPLv3)
        Me.tpGPLv3.Location = New System.Drawing.Point(4, 22)
        Me.tpGPLv3.Name = "tpGPLv3"
        Me.tpGPLv3.Size = New System.Drawing.Size(768, 573)
        Me.tpGPLv3.TabIndex = 7
        Me.tpGPLv3.Text = "GPLv3 License"
        Me.tpGPLv3.UseVisualStyleBackColor = True
        '
        'rtbGPLv3
        '
        Me.rtbGPLv3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtbGPLv3.Location = New System.Drawing.Point(6, 6)
        Me.rtbGPLv3.Name = "rtbGPLv3"
        Me.rtbGPLv3.ReadOnly = True
        Me.rtbGPLv3.Size = New System.Drawing.Size(756, 561)
        Me.rtbGPLv3.TabIndex = 3
        Me.rtbGPLv3.Text = ""
        '
        'Licenses
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 623)
        Me.Controls.Add(Me.TabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Licenses"
        Me.Text = "Licenses"
        Me.TabControl1.ResumeLayout(False)
        Me.tpAbout.ResumeLayout(False)
        Me.tpAbout.PerformLayout()
        Me.tpMIT.ResumeLayout(False)
        Me.tpNLog.ResumeLayout(False)
        Me.tp7Zip.ResumeLayout(False)
        Me.tpApache.ResumeLayout(False)
        Me.tpOpenIcon.ResumeLayout(False)
        Me.tpGPLv3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents tpMIT As TabPage
    Friend WithEvents rtbAutobackup As RichTextBox
    Friend WithEvents tp7Zip As TabPage
    Friend WithEvents rtb7Zip As RichTextBox
    Friend WithEvents tpOpenIcon As TabPage
    Friend WithEvents rtbIcons As RichTextBox
    Friend WithEvents tpAbout As TabPage
    Friend WithEvents rtbAbout As TextBox
    Friend WithEvents tpApache As TabPage
    Friend WithEvents rtbApache As RichTextBox
    Friend WithEvents tpNLog As TabPage
    Friend WithEvents rtbNlog As RichTextBox
    Friend WithEvents tpGPLv3 As TabPage
    Friend WithEvents rtbGPLv3 As RichTextBox
End Class
