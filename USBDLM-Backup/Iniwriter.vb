﻿Imports System.IO
Imports MadMilkman.Ini
' Imports System.Security.Cryptography

Module Iniwriter
    Dim sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    Dim sPaBINIFile As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup\PaB.ini"
    Public Sub WriteIni(ByVal sID As String, ByVal sSourcepaths() As String,
                          Optional ByVal sDescription As String = "",
                          Optional ByVal sPassword As String = Nothing)

        sID = sID.Split("\")(2)
        _AddIniSections(sID, sSourcepaths, sDescription, sPassword)
    End Sub

    '''' <summary>
    '''' Create MD5 checksum of sFilename as String
    '''' </summary>
    '''' <param name="filename"></param>
    '''' <returns></returns>
    'Private Function _CreateMD5(fileName As String) As String
    '    Using md5__2 = MD5.Create()
    '        Using stream = File.OpenRead(fileName)
    '            Return BitConverter.ToString(md5__2.ComputeHash(stream)).Replace("-", String.Empty)
    '        End Using
    '    End Using
    'End Function

    Public Sub _DeleteINISection(ByVal sSectionname As String, ByVal sIdentifier As String)
        Dim ini As New IniFile()

        ini.Load(sPaBINIFile)
        For Each section As IniSection In ini.Sections.Where(Function(s) s.Name = sSectionname)
            For Each key As IniKey In section.Keys
                If key.Value.ToLower.Contains(sIdentifier.ToLower) Then
                    ini.Sections.Remove(section)
                    ini.Save(sPaBINIFile)
                    Exit Sub
                End If
            Next
        Next
    End Sub

    Private Sub _AddIniSections(ByVal sSection As String, ByVal sSourcepaths() As String,
                                Optional ByVal sDescription As String = "", Optional ByVal sPassword As String = "")
        If Not Directory.Exists(Path.GetDirectoryName(sPaBINIFile)) Then
            Directory.CreateDirectory(Path.GetDirectoryName(sPaBINIFile))
        End If

        If Not File.Exists(sPaBINIFile) Then
            ' Write some default settings
            Try
                If Not Directory.Exists(Path.GetDirectoryName(sPaBINIFile)) Then
                    Directory.CreateDirectory(Path.GetDirectoryName(sPaBINIFile))
                End If
                Using swPaBINI As StreamWriter = New StreamWriter(sPaBINIFile)
                    swPaBINI.WriteLine("; PaB INI file")
                    swPaBINI.WriteLine("; created " + DateTime.Today.ToString)
                    swPaBINI.WriteLine("[settings]")
                    swPaBINI.WriteLine("; Activate email reporting")
                    swPaBINI.WriteLine("SendReport=False")
                    swPaBINI.WriteLine("MailgunAPIKey=key-65d52cc570230f0f4e25684c2b2b7238")
                    swPaBINI.WriteLine("MailgunDomain=sandbox2c45884527824533b2ccaae8ded609db.mailgun.org")
                    swPaBINI.WriteLine("MailgunSender=Plug-And-Backup <noreply@butenostfreesen.de>")
                    swPaBINI.WriteLine("; Add one or more receipients")
                    swPaBINI.WriteLine("SendReportTo=me1@mydomain.com")
                    swPaBINI.WriteLine("SendReportTo=me2@mydomain.com")
                    swPaBINI.WriteLine("; Time intervall in seconds to wait before starting the backup jobs (set 0 for no wait)")
                    swPaBINI.WriteLine("WaitForUserBeforeStart=16")
                    swPaBINI.WriteLine("; Time intervall in seconds to wait for pre/post commands")
                    swPaBINI.WriteLine("; Set to 0 for endless (use with caution!)")
                    swPaBINI.WriteLine("PrePostCommandWait=10")
                End Using
            Catch ex As Exception
                MsgBox("Fatal error creating ini file!", MsgBoxStyle.Critical)
                End
            End Try
        End If

        sSection = "Device_" + sSection
        Dim ini As New IniFile()
        Try
            ini.Load(sPaBINIFile)
        Catch ex As Exception

        End Try


        ini.Sections.Add(New IniSection(ini, sSection))
        Dim iCount As Integer = 1
        For Each sPath In sSourcepaths
            ini.Sections(sSection).Keys.Add("Dir", sPath)
            ini.Sections(sSection).Keys.Add("Lastrun" + iCount.ToString, "")
            ini.Sections(sSection).Keys.Add("Laststate" + iCount.ToString, "")
            iCount += 1
        Next
        ini.Sections(sSection).Keys.Add("Password", sPassword)
        ini.Sections(sSection).Keys.Add("Description", sDescription)
        ini.Sections(sSection).Keys.Add("PreCmd", "")
        ini.Sections(sSection).Keys.Add("PostCmd", "")
        ini.Sections(sSection).TrailingComment.EmptyLinesBefore = 2
        ini.Sections(sSection).TrailingComment.Text = "Added by Plug-And-Backup: " + DateAndTime.Now.ToString

        ini.Save(sPaBINIFile)
    End Sub

End Module

