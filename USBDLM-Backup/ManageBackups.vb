﻿Imports System.IO
Imports System.Text
Imports System.Globalization
Imports MadMilkman.Ini

Public Class ManageBackups
    Dim ci As CultureInfo = CultureInfo.InstalledUICulture
    Dim sBatchpath As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup"
    Dim sPaBINIFile As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup\PaB.ini"

    Private Sub ManageBackups_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        _initialize()
        Location = New Point(My.Settings.ManagerPosX, My.Settings.ManagerPosY)
    End Sub

    Private Sub _initialize()
        lbBackups.Items.Clear()
        lbDirectories.Items.Clear()
        lbDescription.Text = ""
        lbLastrun.Text = ""
        lbLaststate.Text = ""
        Dim IniFile As IniFile = New IniFile()
        If File.Exists(sPaBINIFile) Then
            IniFile.Load(sPaBINIFile)

            Dim section As IniSection = Nothing
            Try
                For Each sect In IniFile.Sections()
                    If sect.Name.Contains("Device_") Then
                        section = sect
                        lbBackups.Items.Add(sect.Name)
                    End If
                Next
            Catch ex As Exception
                If ci.Name = "de-DE" Then
                    MsgBox("Es wurden keine Backup-Sets gefunden.", MsgBoxStyle.Information)
                    Close()
                Else
                    MsgBox("No existing backup sets found.", MsgBoxStyle.Information)
                    Close()
                End If
            End Try
        Else
            If ci.Name = "de-DE" Then
                MsgBox("Es wurden keine Backup-Sets gefunden.", MsgBoxStyle.Information)
                Close()
            Else
                MsgBox("No existing backup sets found.", MsgBoxStyle.Information)
                Close()
            End If
        End If
    End Sub

    Private Sub _ParseAndAddDirectories()
        lbDirectories.Items.Clear()
        Dim IniFile As IniFile = New IniFile()
        IniFile.Load(sPaBINIFile)

        Try
            For Each sect In IniFile.Sections()
                If sect.Name = lbBackups.SelectedItem.ToString Then
                    For Each key As IniKey In sect.Keys.Where(Function(k) k.Name = "Dir")
                        lbDirectories.Items.Add(key.Value)
                    Next
                    lbDescription.Text = "Description: " + sect.Keys("Description").Value
                    Exit For
                End If
            Next
        Catch ex As Exception
            If ci.Name = "de-DE" Then
                MsgBox("Es wurden keine Backup-Sets gefunden.", MsgBoxStyle.Information)
                Close()
            Else
                MsgBox("No existing backup sets found.", MsgBoxStyle.Information)
                Close()
            End If
        End Try
    End Sub

    Private Sub lbBackups_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbBackups.SelectedIndexChanged
        lbLastrun.Text = ""
        lbLaststate.Text = ""
        _ParseAndAddDirectories()
    End Sub

    Private Sub DeleteSelectionToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DeleteSelectionToolStripMenuItem.Click
        Dim IniFile As IniFile = New IniFile()
        IniFile.Load(sPaBINIFile)
        Dim section = IniFile.Sections(lbBackups.SelectedItem.ToString)
        Try
            IniFile.Sections.Remove(section)
        Catch ex As Exception

        End Try
        IniFile.Save(sPaBINIFile)
        _initialize()
    End Sub

    Private Sub lbDirectories_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lbDirectories.SelectedIndexChanged
        Dim IniFile As IniFile = New IniFile()
        IniFile.Load(sPaBINIFile)
        Dim sLastrun As String = ""
        Try
            For Each sect In IniFile.Sections()
                If sect.Name = lbBackups.SelectedItem.ToString Then
                    sLastrun = sect.Keys("Lastrun" + (lbDirectories.SelectedIndex + 1).ToString).Value
                    If sLastrun <> "" Then
                        lbLastrun.Text = " | Last Run: " + sLastrun
                        lbLaststate.Text = " | Last Status: " + sect.Keys("Laststate" + (lbDirectories.SelectedIndex + 1).ToString).Value
                    End If
                    Exit For
                End If
            Next
            If sLastrun = "" Then
                lbLastrun.Text = " | Last Run: No run yet"
                lbLaststate.Text = " | Last Status: n/a"
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub ManageBackups_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        My.Settings.ManagerPosX = Location.X
        My.Settings.ManagerPosY = Location.Y
    End Sub
End Class