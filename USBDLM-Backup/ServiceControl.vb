﻿Imports System.IO
Public Class ServiceControl
    Public Shared sServiceName As String = "PlugAndBackupService"
    Public Shared sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    Public Shared Function _start()
        Return _launchProcess("sc.exe", "start " + sServiceName)
    End Function

    Public Shared Function _stop()
        Return _launchProcess("sc.exe", "stop " + sServiceName)
    End Function

    Public Shared Function _register()
        Dim sBinPath As String = """" + sBasepath + "\Plug-And-Backup-Service.exe"""
        Return _launchProcess("sc.exe", String.Format("create {0} start=auto binPath={1}", sServiceName, sBinPath))
    End Function

    Public Shared Function _deregister()
        Return _launchProcess("sc.exe", "delete " + sServiceName)
    End Function

    Private Shared Function _launchProcess(ByVal sCmd As String, ByVal sArgs As String) As Integer
        Try
            Dim prMyProcess As Process = New Process
            With prMyProcess.StartInfo
                .WorkingDirectory = sBasepath
                .UseShellExecute = True
                .CreateNoWindow = True
                .WindowStyle = ProcessWindowStyle.Hidden
                .FileName = sCmd
                .Arguments = sArgs
                .Verb = "runas"
            End With
            prMyProcess.Start()
            prMyProcess.WaitForExit()
            Return prMyProcess.ExitCode
        Catch ex As Exception
            Return -1
        End Try
    End Function
End Class
