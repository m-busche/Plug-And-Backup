﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgenden 
' Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
' die einer Assembly zugeordnet sind.

' Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("Plug-And-Backup")>
<Assembly: AssemblyDescription("Fully Automated Plug-And-Play Backup Using USBDLM")>
<Assembly: AssemblyCompany("Markus Busche")>
<Assembly: AssemblyProduct("Plug-And-Backup")>
<Assembly: AssemblyCopyright("Copyright ©  2017 elpatron@butenostfreesen.de")>
<Assembly: AssemblyTrademark("")>

<Assembly: ComVisible(False)>

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird.
<Assembly: Guid("05d6d6c5-7b5c-40f1-ac48-4b011564ae93")>

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.6.1")>
<Assembly: AssemblyFileVersion("1.6.1")>
