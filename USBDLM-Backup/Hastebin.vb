﻿Imports System.IO
Imports System.Net
Imports System.Text.RegularExpressions

Namespace butenostfreesen
    Public Class Hastebin
        Private Shared ReadOnly _HasteKeyRegex As New Regex("{""key"":""(?<key>[a-z].*)""}", RegexOptions.Compiled)

        <STAThread>
        Public Shared Function SendFile(sFilename As String)

            Dim haste As String = Nothing
            If Not [String].IsNullOrEmpty(sFilename) Then
                haste = File.ReadAllText(sFilename)
            End If

            Using client = New WebClient()
                Dim response = client.UploadString("https://haste.butenostfreesen.de/documents", haste)
                Dim match = _HasteKeyRegex.Match(response)

                If Not match.Success Then
                    Console.WriteLine(response)
                    Return Nothing
                End If

                Dim hasteUrl As String = [String].Concat("https://haste.butenostfreesen.de", "/", match.Groups("key"))

                Return hasteUrl
            End Using
        End Function

    End Class
End Namespace