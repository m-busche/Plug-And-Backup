﻿Imports System.Management
Imports Plug_And_Backup_Service

Module getdevice
    Public Function getvolumes() As List(Of String)
        Dim mydevices As New List(Of USBDeviceInfo)()
        Dim Devices As New List(Of String)
        mydevices = GetUSBDevices()

        For Each udevice In mydevices
            Devices.Add(udevice.DeviceID.ToString)
        Next
        Devices = _Distinct(Devices)
        Return Devices
    End Function

    Private Function _Distinct(Of T)(list As List(Of T)) As List(Of T)
        Return (New HashSet(Of T)(list)).ToList()
    End Function

    Private Function GetUSBDevices() As List(Of USBDeviceInfo)
        Dim devices As New List(Of USBDeviceInfo)()

        Dim collection As ManagementObjectCollection
        Using searcher = New ManagementObjectSearcher("Select * From Win32_USBHub")
            collection = searcher.[Get]()
        End Using

        For Each device In collection
            devices.Add(New USBDeviceInfo(DirectCast(device.GetPropertyValue("DeviceID"), String), DirectCast(device.GetPropertyValue("PNPDeviceID"), String), DirectCast(device.GetPropertyValue("Description"), String)))
        Next

        collection.Dispose()
        Return devices
    End Function

End Module
