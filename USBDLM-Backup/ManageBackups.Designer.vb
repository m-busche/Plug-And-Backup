﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManageBackups
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManageBackups))
        Me.lbBackups = New System.Windows.Forms.ListBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DeleteSelectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lbDirectories = New System.Windows.Forms.ListBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lbDescription = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbLastrun = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lbLaststate = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbBackups
        '
        resources.ApplyResources(Me.lbBackups, "lbBackups")
        Me.lbBackups.ContextMenuStrip = Me.ContextMenuStrip1
        Me.lbBackups.FormattingEnabled = True
        Me.lbBackups.Name = "lbBackups"
        Me.ToolTip1.SetToolTip(Me.lbBackups, resources.GetString("lbBackups.ToolTip"))
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DeleteSelectionToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        resources.ApplyResources(Me.ContextMenuStrip1, "ContextMenuStrip1")
        '
        'DeleteSelectionToolStripMenuItem
        '
        Me.DeleteSelectionToolStripMenuItem.Name = "DeleteSelectionToolStripMenuItem"
        resources.ApplyResources(Me.DeleteSelectionToolStripMenuItem, "DeleteSelectionToolStripMenuItem")
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'lbDirectories
        '
        resources.ApplyResources(Me.lbDirectories, "lbDirectories")
        Me.lbDirectories.FormattingEnabled = True
        Me.lbDirectories.Name = "lbDirectories"
        Me.ToolTip1.SetToolTip(Me.lbDirectories, resources.GetString("lbDirectories.ToolTip"))
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lbDescription, Me.lbLastrun, Me.lbLaststate})
        resources.ApplyResources(Me.StatusStrip1, "StatusStrip1")
        Me.StatusStrip1.Name = "StatusStrip1"
        '
        'lbDescription
        '
        Me.lbDescription.Name = "lbDescription"
        resources.ApplyResources(Me.lbDescription, "lbDescription")
        '
        'lbLastrun
        '
        Me.lbLastrun.Name = "lbLastrun"
        resources.ApplyResources(Me.lbLastrun, "lbLastrun")
        '
        'lbLaststate
        '
        Me.lbLaststate.Name = "lbLaststate"
        resources.ApplyResources(Me.lbLaststate, "lbLaststate")
        '
        'ManageBackups
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lbDirectories)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lbBackups)
        Me.Name = "ManageBackups"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lbBackups As ListBox
    Friend WithEvents Label1 As Label
    Friend WithEvents lbDirectories As ListBox
    Friend WithEvents Label2 As Label
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents DeleteSelectionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents lbLastrun As ToolStripStatusLabel
    Friend WithEvents lbLaststate As ToolStripStatusLabel
    Friend WithEvents lbDescription As ToolStripStatusLabel
End Class
