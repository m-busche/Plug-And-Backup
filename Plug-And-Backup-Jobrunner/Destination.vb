﻿Imports System.IO
Public Class backupset
    Private sDrive As String
    Private sFolders As List(Of String)
    Private sFilename As String
    Private sDescription As String
    Private sSourcepaths As List(Of String)
    Private bFullbackup As Boolean

    Public Sub New(Optional ByVal sDrive As String = Nothing,
                   Optional ByVal sFolders As List(Of String) = Nothing,
                   Optional ByVal sFilename As String = Nothing,
                   Optional ByVal sDescription As String = Nothing,
                   Optional ByVal sSourcepaths As List(Of String) = Nothing,
                   Optional ByVal bFullbackup As Boolean = False)
        Me.sDrive = sDrive
        Me.sFolders = sFolders
        Me.sFilename = sFilename
        Me.sDescription = sDescription
        Me.sSourcepaths = sSourcepaths
        Me.bFullbackup = bFullbackup
    End Sub

    Public Property Drive As String
        Get
            Return Me.sDrive
        End Get
        Set(ByVal Value As String)
            Me.sDrive = Value
        End Set
    End Property

    Public Property Folders As List(Of String)
        Get
            Return Me.sFolders
        End Get
        Set(ByVal Value As List(Of String))
            Me.sFolders = Value
        End Set
    End Property

    Public Property Filename As String
        Get
            Return Me.sFilename
        End Get
        Set(ByVal Value As String)
            Me.sFilename = Value
        End Set
    End Property

    Public Property Description As String
        Get
            Return Me.sDescription
        End Get
        Set(ByVal Value As String)
            Me.sDescription = Value
        End Set
    End Property

    Public Property Sourcepaths As List(Of String)
        Get
            Return Me.sSourcepaths
        End Get
        Set(ByVal values As List(Of String))
            Me.sSourcepaths = values
            For Each myval In values
                Me.Folders.Add(myval.Split("\")())
            Next
        End Set
    End Property

    Public Property Fullbackup As Boolean
        Get
            Return Me.bFullbackup
        End Get
        Set(value As Boolean)
            Me.bFullbackup=value
        End Set
    End Property

    Public ReadOnly Property FullPath() As String
        Get
            Return Path.Combine(sDrive, sFolder, sFilename)
        End Get
    End Property

End Class
