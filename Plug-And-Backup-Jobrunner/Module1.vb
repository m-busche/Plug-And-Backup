﻿' https://app.mailgun.com/new/signup/
' https://github.com/lukencode/FluentEmail/tree/master/src/Senders/FluentEmail.Mailgun

Imports System.IO
Imports System.Threading
Imports NLog
Imports NLog.Targets
Imports NLog.Config
Imports MadMilkman.Ini
Imports RestSharp

Module Main
    Public sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
    Public sDataPath As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup"
    Public sPaBINIFile As String = sDataPath + "\PaB.ini"
    Private logger As Logger = LogManager.GetCurrentClassLogger()
    Private sUSBLogFileName As String

    Private Structure devicestate
        Public lFreeBytesBeforeBackup As Long
        Public lFreeBytesAfterBackup As Long
    End Structure

    Private Structure backupresult
        Public Sourcepath As String
        Public Result As Integer
        Public Logfile As String
        Public iNumSourceFiles As Integer
        Public lSizeSourceFiles As Long
        Public lTotalArchivesSize As Long
        Public lLastArchivesSize As Long
        Public iNumArchives As Integer
        Public dCompressionRatio As Decimal

        Public Sub calcCompression()
            Try
                dCompressionRatio = lTotalArchivesSize / lSizeSourceFiles
            Catch ex As Exception
                dCompressionRatio = -1
            End Try
        End Sub
    End Structure

    Sub Main()
        Dim config = New LoggingConfiguration()
        Dim ftLocalFileTarget = New FileTarget()
        config.AddTarget("file", ftLocalFileTarget)
        ftLocalFileTarget.Layout = "${date:format=HH\:mm\:ss} ${logger} ${message}"
        ftLocalFileTarget.FileName = "${specialfolder:folder=CommonApplicationData}\Plug-And-Backup\logs\Jobrunner_${shortdate}.log"
        Dim rule = New LoggingRule("*", LogLevel.Debug, ftLocalFileTarget)
        config.LoggingRules.Add(rule)
        LogManager.Configuration = config
        Dim sVersion As String = String.Format("Version {0}", My.Application.Info.Version.ToString)

        logger.Info("Starting Jobrunner " + sVersion)
        Dim sDeviceID As String = Nothing
        Dim sDrive As String = Nothing
        Dim sLaunchfile As String = sDataPath + "\launch.txt"
        Try
            ' Read job information from file
            logger.Debug("Reading launch information from " + sLaunchfile)
            Using sr As StreamReader = New StreamReader(sLaunchfile)
                sDeviceID = sr.ReadLine
                sDrive = sr.ReadLine
            End Using
            ' Add new log target on USB device
            Dim ftUSBFileTarget = New FileTarget()
            config.AddTarget("file", ftUSBFileTarget)
            ftUSBFileTarget.Layout = "${date:format=HH\:mm\:ss} ${logger} ${message}"
            sUSBLogFileName = String.Format("{0}:\pab-logs\Jobrunner_{1:yyyy-MM-dd_HH-mm-ss}_log.txt", sDrive, DateTime.Now)
            ftUSBFileTarget.FileName = sUSBLogFileName
            Dim USBrule = New LoggingRule("*", LogLevel.Debug, ftUSBFileTarget)
            config.LoggingRules.Add(USBrule)
            logger.Info("Logfile on USB device initiated (" + sVersion + ")")
        Catch ex As Exception
            logger.Warn("Failed getting information about backup job: " + ex.Message)
            Console.WriteLine("Failed getting information about backup job, see log files for more information!")
            Exit Sub
        End Try
        LogManager.ReconfigExistingLoggers()
        Try
            File.Delete(sLaunchfile)
        Catch ex As Exception
            logger.Debug("Deleting launch file " + sLaunchfile + " failed: " + ex.Message)
        End Try
        logger.Info(String.Format("Launching backup to device {0} (Driveletter: {1})", sDeviceID, sDrive))
        _backup(sDeviceID, sDrive)
    End Sub

    Private Sub _intro(waitforuser As Integer)
        Console.WriteLine("Plug-And-Backup Jobrunner")
        Console.WriteLine()
        Console.WriteLine("Please be patient until the process finishes.")
        Console.WriteLine("Do not unplug your USB device or close this window during the backup!")
        Console.WriteLine()
        Console.WriteLine()

        If waitforuser <> 0 Then
            Console.WriteLine(String.Format("You have {0} seconds to cancel (via Crtl-C)", waitforuser.ToString))
            Dim origRow As Integer = Console.CursorTop
            Dim origCol As Integer = Console.CursorLeft
            For iCount As Integer = waitforuser - 1 To 0 Step -1
                Console.SetCursorPosition(origCol, origRow)
                Console.WriteLine(iCount.ToString + "  ")
                Thread.Sleep(1000)
            Next
            ' Overwrite counter
            Console.SetCursorPosition(origCol, origRow - 1)
            Console.WriteLine("                                             ")
            Console.SetCursorPosition(origCol, origRow)
            Console.WriteLine("                                             ")
        End If
    End Sub

    Private Sub _backup(sDeviceID As String, sDrive As String)
        Dim sLockfile As String = sDrive + ":\PaB.lock"
        Dim PaBini As IniFile = New IniFile()

        Try
            PaBini.Load(sPaBINIFile)
            logger.Debug("Loaded INI file: " + sPaBINIFile)
        Catch ex As Exception
            logger.Error("Failed loading ini file " + sPaBINIFile + ": " + ex.Message)
            _DeleteLockfile(sLockfile)
            Exit Sub
        End Try


        If Not File.Exists(sLockfile) Then
            logger.Debug("Creating lock file")
            Using sw As StreamWriter = New StreamWriter(sLockfile)
                sw.WriteLine()
            End Using
            Dim settings As IniSection = PaBini.Sections("settings")
            Dim waitbeforestart As Integer = 16

            Try
                waitbeforestart = settings.Keys("WaitForUserBeforeStart").Value
                logger.Debug("Setting 'waitbeforestart' to " + waitbeforestart.ToString)
            Catch ex As Exception

            End Try
            _intro(waitbeforestart)
        Else
            logger.Warn("Backup halted, lock file found on device.")
            Console.WriteLine("There seems to be another backup task running on this device or the last process was canceled unexpextedly.")
            Console.WriteLine()
            Try
                Console.WriteLine("If no other task is running: Hit <y> and <ENTER> to proceed.")
                Console.WriteLine("If no answer is given in 10 seconds, the process will be canceled.")
                Try
                    Dim answer As String = ConsoleReader.ReadLine(10000)
                    If answer = "y" Then
                        Try
                            File.Delete(sLockfile)
                            _backup(sDeviceID, sDrive)
                        Catch ex As Exception
                            logger.Debug("Failed deleting " + sLockfile)
                        End Try
                    Else
                        logger.Info("User canceled process [Lock file]")
                        _WriteDoneFile(sDeviceID)
                    End If
                Catch ex As Exception
                    ' No input from user
                End Try
            Catch generatedExceptionName As TimeoutException
                Console.WriteLine("You waited too long, the backup was canceled.")
                logger.Info("Backup canceled [Lock file, timeout]")
                _WriteDoneFile(sDeviceID)
            End Try

            Exit Sub
        End If

        Dim sIDFile As String = sDrive + ":\PaB_Device-ID_" + sDeviceID + ".txt"
        If Not File.Exists(sIDFile) Then
            Using sw As StreamWriter = New StreamWriter(sIDFile)
                sw.WriteLine()
            End Using
            logger.Debug("Creating ID file")
        End If

        Dim sSourcePaths As List(Of String) = New List(Of String)
        Dim sDescription As String = Nothing
        Dim sPassword As String = Nothing
        Dim sArgs As String = Nothing
        Dim sDestinationPath As String = Nothing

        Dim section As IniSection = Nothing
        For Each sect In PaBini.Sections()
            If sect.Name.Contains(sDeviceID) Then
                section = sect
                logger.Debug("Device section matches ini section")
                Exit For
            End If
        Next

        If section.Name = Nothing Then
            logger.Warn("No matching ini section found")
            Exit Sub
        End If

        ' read source paths
        For Each key As IniKey In section.Keys.Where(Function(k) k.Name = "Dir")
            sSourcePaths.Add(key.Value)
        Next

        If sSourcePaths.Count = 0 Then
            logger.Warn("No source paths defined")
            Exit Sub
        End If

        sDescription = section.Keys("Description").Value
        Try
            sPassword = section.Keys("Password").Value
        Catch ex As Exception
            sPassword = Nothing
        End Try


        Dim sourceid As String = Nothing
        Dim icount As Integer = 1
        Dim results(sSourcePaths.Count) As backupresult

        ' Get available disk space on destination device before backup
        Dim Destinationstate As New devicestate
        Destinationstate.lFreeBytesBeforeBackup = _GetAvailableDiskSpace(sDrive)

        For Each sourcepath In sSourcePaths
            Dim result As New backupresult
            result.Sourcepath = sourcepath
            Console.WriteLine(String.Format("Starting backup {0} of {1}.", icount.ToString, sSourcePaths.Count.ToString))

            ' Count source directory files and bytes
            Console.WriteLine("Counting source files. Please wait...")
            result.iNumSourceFiles = Directory.GetFiles(sourcepath, "*", SearchOption.AllDirectories).Length
            result.lSizeSourceFiles = Directory.GetFiles(sourcepath, "*", SearchOption.AllDirectories).Sum(Function(t) (New FileInfo(t).Length))
            Console.WriteLine("Total file count: " + result.iNumSourceFiles.ToString)
            Console.WriteLine("Total file size: " + result.lSizeSourceFiles.ToString + " bytes")

            logger.Debug("Starting backup for " + sourcepath)
            logger.Debug("File count: " + result.iNumSourceFiles.ToString)
            logger.Debug("Total file size: " + result.lSizeSourceFiles.ToString)
            Console.WriteLine("Source path: " + sourcepath)
            sourceid = sourcepath.Substring(0, 1) + "_" +
                sourcepath.Substring(sourcepath.LastIndexOf("\") + 1)
            sourceid = RemoveSpecialCharacters.RemoveSpecialChars(sourceid)
            sDestinationPath = String.Format("{0}:\Plug-And-Backup\{1}", sDrive, sourceid)
            logger.Debug("Destination path: " + sDestinationPath)
            If Not Directory.Exists(sDestinationPath) Then
                Try
                    Directory.CreateDirectory(sDestinationPath)
                    logger.Debug("Creating destination directory")
                Catch ex As Exception
                    logger.Error("Failed creating directory: " + sDestinationPath)
                    Exit For
                End Try
            End If
            Dim sTimestamp As String = DateTime.Now.ToString("yyyyMMddHHmmss")
            Dim sFullbackupZipfilename As String = String.Format("{0}\PaB-Full_ID_{1}.7z", sDestinationPath, sDeviceID)
            Dim sDiffbackupZipfilename As String = String.Format("{0}\PaB-Diff_ID_{1}_{2}.7z", sDestinationPath, sDeviceID, sTimestamp)
            Dim bFullbackup As Boolean = False

            If Not File.Exists(sFullbackupZipfilename) Then
                ' create full backup
                bFullbackup = True
                logger.Debug("Creating full backup")
                If sPassword = "" Then
                    logger.Debug("Password is not set")
                    sArgs = String.Format("a ""{0}"" ""{1}"" -ms=off", sFullbackupZipfilename, sourcepath)
                Else
                    logger.Debug("Password is set")
                    sArgs = String.Format("a ""{0}"" ""{1}"" -ms=off -p""{2}""", sFullbackupZipfilename, sourcepath, sPassword)
                End If
            Else
                logger.Debug("Creating differential backup")
                ' create differential backup
                If sPassword = "" Then
                    logger.Debug("Password is not set")
                    sArgs = String.Format("u ""{0}"" ""{1}"" -u- -up0q3r2x2y2z0w2!""{2}""", sFullbackupZipfilename, sourcepath,
                                          sDiffbackupZipfilename)
                Else
                    logger.Debug("Password is set")
                    sArgs = String.Format("u ""{0}"" ""{1}"" -p""{2}"" -u- -up0q3r2x2y2z0w2!""{3}""", sFullbackupZipfilename, sourcepath, sPassword,
                                          sDiffbackupZipfilename)
                End If
            End If
            Dim iResult As Integer = 0
            If sPassword <> "" Then
                SevenZipProcessLauncher.startprocess(sArgs, False)
            Else
                SevenZipProcessLauncher.startprocess(sArgs)
            End If
            result.Result = iResult
            section.Keys("Lastrun" + icount.ToString).Value = DateTime.Now.ToString

            ' Gather some statistics
            result.iNumArchives = Directory.GetFiles(sDestinationPath, "*.7z", SearchOption.TopDirectoryOnly).Length
            logger.Debug("Number of archive(s): " + result.iNumArchives.ToString)
            result.lTotalArchivesSize = Directory.GetFiles(sDestinationPath, "*.7z", SearchOption.TopDirectoryOnly).Sum(Function(t) (New FileInfo(t).Length))
            logger.Debug("Total archive(s) file size: " + result.lTotalArchivesSize.ToString + " bytes")
            If bFullbackup = False Then
                result.lLastArchivesSize = Directory.GetFiles(sDestinationPath, Path.GetFileName(sDiffbackupZipfilename),
                                                              SearchOption.TopDirectoryOnly).Sum(Function(t) (New FileInfo(t).Length))
            Else
                result.lLastArchivesSize = result.lTotalArchivesSize
            End If
            logger.Debug("Last archive file size: " + result.lLastArchivesSize.ToString + " bytes")

            result.calcCompression()

            ' Save result
            results(icount - 1) = result

            Console.Write(String.Format("Backup {0} of {1}: ", icount.ToString, sSourcePaths.Count.ToString))
            Select Case iResult
                Case 0
                    Console.WriteLine("Successfull.")
                    section.Keys("Laststate" + icount.ToString).Value = "0 - OK"
                Case 1
                    Console.WriteLine("Warning, see log for details")
                    section.Keys("Laststate" + icount.ToString).Value = "1 - Warning, some files were locked and could not be processed."
                Case 2
                    Console.WriteLine("Fatal error, see log for details")
                    section.Keys("Laststate" + icount.ToString).Value = "2 - Fatal error, see log for details"
                Case 7
                    Console.WriteLine("Command line error")
                    section.Keys("Laststate" + icount.ToString).Value = "7 - Command line error"
                Case 8
                    Console.WriteLine("Not enough memory for operation")
                    section.Keys("Laststate" + icount.ToString).Value = "8 - Not enough memory for operation"
                Case 255
                    Console.WriteLine("User stopped the process")
                    section.Keys("Laststate" + icount.ToString).Value = "255 - User stopped the process"
                Case Else
                    Console.WriteLine("Unknown exit code, see log for details")
                    section.Keys("Laststate" + icount.ToString).Value = "Unknown exit code, see log for details"
            End Select
            Console.WriteLine()
            logger.Info("7-Zip exit code: " + iResult.ToString)
            icount += 1
        Next
        PaBini.Save(sPaBINIFile)

        ' Get available disk space on destination device after backup
        Destinationstate.lFreeBytesAfterBackup = _GetAvailableDiskSpace(sDrive)

        ' Copy 7-Zip Portable to device
        If Not File.Exists(sDrive + ":\7-ZipPortable\7-ZipPortable.exe") Then
            If File.Exists(sBasepath + "\7-ZipPortable\7-ZipPortable.exe") Then
                Try
                    CopyDir.Copy(sBasepath + "\7-ZipPortable", sDrive + ":\7-ZipPortable")
                Catch ex As Exception
                    logger.Info("Failed copying 7-Zip Portable to backup device: " + ex.Message)
                End Try
            End If
        End If

        ' Copy Plug-And-Backup-Restore.exe to device
        Try
            File.WriteAllBytes(sDrive + ":\Plug-And-Backup-Restore.exe", My.Resources.Plug_And_Backup_Restore)
            Directory.CreateDirectory(sDrive + ":\de")
            File.WriteAllBytes(sDrive + ":\de\Plug-And-Backup-Restore.resources.dll", My.Resources.Plug_And_Backup_Restore_resources)
            logger.Debug("Copied Plug-And-Backup-Restore to backup device")
        Catch ex As Exception
            logger.Info("Failed copying Plug-And-Backup-Restore to backup device: " + ex.Message)
        End Try

        ' Create autorun.inf
        Dim sAutorunfile As String = sDrive + ":\autorun.inf"
        If Not File.Exists(sAutorunfile) Then
            logger.Debug("Creating " + sAutorunfile)
            Try
                Using swAutorun As StreamWriter = New StreamWriter(sAutorunfile)
                    swAutorun.WriteLine("[AutoRun]")
                    swAutorun.WriteLine("OPEN=Plug-And-Backup-Restore.exe")
                    swAutorun.WriteLine("ACTION=Start Plug-And-Backup-Restore")
                    swAutorun.WriteLine("LABEL=" + sDeviceID)
                    logger.Debug("Created " + sAutorunfile)
                End Using
            Catch ex As Exception
                logger.Debug("Failed creating " + sAutorunfile)
            End Try
        End If

        ' Delete lock file
        _DeleteLockfile(sLockfile)

        ' Write *.done file as signal for service / post cmd
        _WriteDoneFile(sDeviceID)

        ' Get remaining free disk space on backup device and send email report
        Destinationstate.lFreeBytesAfterBackup = _GetAvailableDiskSpace(sDrive)
        logger.Info("Remaining free disk space on backup device: " + Destinationstate.lFreeBytesAfterBackup.ToString + " bytes")
        _SendMailReport(results, sDeviceID, Destinationstate.lFreeBytesBeforeBackup, Destinationstate.lFreeBytesAfterBackup)

        ' Wait 10 Seconds before closing
        Console.WriteLine("Hit [ENTER] to close this window (or wait 10 seconds).")
        Try
            ConsoleReader.ReadLine(10000)
        Catch ex As Exception

        End Try
    End Sub

    Private Function _GetAvailableDiskSpace(sDrive As String) As Long
        Try
            Dim d As DriveInfo = New DriveInfo(sDrive + ":\")
            Return d.AvailableFreeSpace
            logger.Debug("Getting available free disk space on " + sDrive)
        Catch ex As Exception
            logger.Debug("Failed getting available free disk space: " + ex.Message)
            Return -1
        End Try
    End Function

    Private Sub _SendMailReport(results() As backupresult, sDeviceID As String, Optional ByVal FreeDiskSpaceBefore As Long = 0,
                                Optional ByVal FreeDiskSpaceAfter As Long = 0)
        Dim sText As String = String.Format("Plug-And-Backup report" + vbCrLf + vbCrLf +
                              "The following backups have been run at {0}:" + vbCrLf, DateTime.Now.ToString)
        sText += "Backup device ID: " + sDeviceID + vbCrLf
        If FreeDiskSpaceBefore > 0 Then
            sText += "Remaining free disk space on backup device before backup: " + FreeDiskSpaceBefore.ToString + " bytes" + vbCrLf
            sText += "Remaining free disk space on backup device after backup: " + FreeDiskSpaceAfter.ToString + " bytes" + vbCrLf
        End If
        sText += vbCrLf
        sText += "Source path(s) and result(s):" + vbCrLf +
                 "========================================================" + vbCrLf

        For Each r As backupresult In results
            If r.Sourcepath <> Nothing Then
                sText += "Sourcepath: " + r.Sourcepath + vbCrLf +
                    "Result: " + r.Result.ToString + vbCrLf +
                    "Total source file count: " + r.iNumSourceFiles.ToString + vbCrLf +
                    "Total source file size: " + r.lSizeSourceFiles.ToString + " bytes" + vbCrLf +
                    "Number of archive files: " + r.iNumArchives.ToString + vbCrLf +
                    "Total Archive(s) file size: " + r.lTotalArchivesSize.ToString + " bytes" + vbCrLf +
                    "This Archive file size: " + r.lLastArchivesSize.ToString + " bytes" + vbCrLf +
                    "Compression ratio (total archive(s) size / total source size): " + Math.Round(r.dCompressionRatio, 2).ToString + vbCrLf +
                    "________________________________________________________" + vbCrLf
            End If
        Next
        sText += vbCrLf
        sText += "Result code explanation from 7-Zip documentation:" + vbCrLf +
            "0:   No error" + vbCrLf +
            "1:   Warning (Non fatal error(s)). For example, one or more files were locked by some other application, so they were not compressed." + vbCrLf +
            "2:   Fatal error" + vbCrLf +
            "7:   Command line error" + vbCrLf +
            "8:   Not enough memory for operation" + vbCrLf +
            "255: User stopped the process"
        ' Get receipients from INI file
        Dim IniFile As IniFile = New IniFile()
        Try
            logger.Debug("Opening INI file for getting email receipients: " + sPaBINIFile)
            IniFile.Load(sPaBINIFile)
        Catch ex As Exception
            logger.Debug("Failed opening INI file: " + ex.Message)
            Exit Sub
        End Try
        Try
            Dim settings As IniSection = IniFile.Sections("Report")
            If settings.Keys("SendReport").Value = "True" Then
                Dim sApiKey As String = settings.Keys("MailgunAPIKey").Value
                Dim sDomain As String = settings.Keys("MailgunDomain").Value
                Dim sSenderAdress As String = settings.Keys("MailgunSender").Value
                Dim receipients As List(Of String) = New List(Of String)
                For Each key As IniKey In settings.Keys
                    If key.Name = "SendReportTo" Then
                        receipients.Add(key.Value)
                    End If
                Next
                If receipients.Count > 0 And sApiKey <> "" And sDomain <> "" Then
                    logger.Info("Sending report email(s)")
                    MailGun._SendMailgunMail(receipients, sText, sUSBLogFileName, sApiKey, sDomain, sSenderAdress)
                Else
                    logger.Debug("No receipients found.")
                End If
            End If
        Catch ex As Exception
            logger.Debug("Failed getting email receipients or Mailgun settings from INI file: " + ex.Message)
            Exit Sub
        End Try
    End Sub

    Private Sub _DeleteLockfile(sLockfile As String)
        If File.Exists(sLockfile) Then
            Try
                File.Delete(sLockfile)
                logger.Debug("Lock file deleted")
            Catch ex As Exception
                logger.Debug("Failed deleting lock file")
            End Try
        End If
    End Sub

    Private Sub _WriteDoneFile(sDeviceID As String)
        Try
            Dim sFilename As String = sDataPath + "\Job_" + sDeviceID + ".done"
            logger.Info("Writing done file: " + sFilename)
            Using swDonefile As StreamWriter = New StreamWriter(sFilename)
                swDonefile.WriteLine()
            End Using
        Catch ex As Exception
            logger.Warn("Failed writing done file: " + ex.Message)
        End Try
    End Sub

End Module
