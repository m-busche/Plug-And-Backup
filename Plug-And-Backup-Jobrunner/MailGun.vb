﻿Imports RestSharp
Imports RestSharp.Authenticators
Imports NLog

Public Class MailGun
    Public Shared Sub _SendMailgunMail(sReceipients As List(Of String), sText As String, sLogfile As String,
                                       sAPIKey As String, sDomain As String, sSender As String)
        Dim logger As Logger = LogManager.GetCurrentClassLogger()
        logger.Debug("Sending mail report")
        Dim client As RestClient = New RestClient
        client.BaseUrl = New Uri("https://api.mailgun.net/v3")
        client.Authenticator = New HttpBasicAuthenticator("api", sAPIKey)
        Dim request As RestRequest = New RestRequest
        request.AddParameter("domain", sDomain, ParameterType.UrlSegment)
        request.Resource = "{domain}/messages"
        request.AddParameter("from", sSender)
        For Each r In sReceipients
            logger.Debug("Adding receipient: " + r)
            request.AddParameter("to", r)
        Next
        request.AddParameter("subject", "Plug-And-Backup report")
        request.AddParameter("text", sText)
        logger.Debug("Attaching log file: " + sLogfile)
        request.AddFile("attachment", sLogfile)
        request.Method = Method.POST

        Try
            Dim requestresult = client.Execute(request)
            logger.Debug("Successfully delivered mail report")
        Catch ex As Exception
            logger.Warn("Failed sending mail: " + ex.Message)
        End Try
    End Sub
End Class
