﻿Imports System.IO
Imports NLog

Public Class SevenZipProcessLauncher
    Public Shared Function startprocess(ByVal sArgs As String, Optional ByVal bLog As Boolean = True) As Integer
        Dim logger As Logger = LogManager.GetCurrentClassLogger()
        Dim iResult As Integer = -1
        Dim sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
        Dim pr7zip As Process = New Process
        With pr7zip.StartInfo
            .UseShellExecute = False
            .CreateNoWindow = True
            .FileName = sBasepath + "\7-ZipPortable\App\7-Zip\7z.exe"
            .Arguments = sArgs
            .RedirectStandardOutput = True
        End With
        logger.Debug("Process launched: " + pr7zip.StartInfo.FileName)
        If bLog = True Then
            logger.Debug("Arguments: " + sArgs)
        Else
            logger.Debug("Arguments: Hidden (password protected archive)")
        End If

        Try
            pr7zip.Start()
            pr7zip.WaitForExit()
            iResult = pr7zip.ExitCode
            logger.Info("7-Zip exit code: " + iResult.ToString)
        Catch ex As Exception
            logger.Warn(ex.Message)
        End Try
        Return iResult
    End Function
End Class
