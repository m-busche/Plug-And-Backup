﻿Imports System.IO

Public Class CopyDir
    Public Shared Sub Copy(sourceDirectory As String, targetDirectory As String)
        Dim diSource As New DirectoryInfo(sourceDirectory)
        Dim diTarget As New DirectoryInfo(targetDirectory)

        CopyAll(diSource, diTarget)
    End Sub

    Public Shared Sub CopyAll(source As DirectoryInfo, target As DirectoryInfo)
        Directory.CreateDirectory(target.FullName)

        ' Copy each file into the new directory.
        For Each fi As FileInfo In source.GetFiles()
            ' Console.WriteLine("Copying {0}\{1}", target.FullName, fi.Name)
            fi.CopyTo(Path.Combine(target.FullName, fi.Name), True)
        Next

        ' Copy each subdirectory using recursion.
        For Each diSourceSubDir As DirectoryInfo In source.GetDirectories()
            Dim nextTargetSubDir As DirectoryInfo = target.CreateSubdirectory(diSourceSubDir.Name)
            CopyAll(diSourceSubDir, nextTargetSubDir)
        Next
    End Sub

    Public Shared Sub Main()
        Dim sourceDirectory As String = "c:\sourceDirectory"
        Dim targetDirectory As String = "c:\targetDirectory"

        Copy(sourceDirectory, targetDirectory)
    End Sub

    ' Output will vary based on the contents of the source directory.
End Class
