[Files]
Source: "..\USBDLM-Backup\bin\Release\de\Plug-And-Backup.resources.dll"; DestDir: "{app}\de"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\MadMilkman.Ini.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\NLog.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\NLog.xml"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\Plug-And-Backup.exe"; DestDir: "{app}"; Flags: ignoreversion
; Source: "..\USBDLM-Backup\bin\Release\PaB.ini"; DestDir: "{commonappdata}\Plug-And-Backup"; Flags: confirmoverwrite onlyifdoesntexist
Source: "..\USBDLM-Backup\bin\Release\README.md"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\Plug-And-Backup-Service.exe"; DestDir: "{app}"; Flags: ignoreversion; BeforeInstall: StopService
Source: "..\USBDLM-Backup\bin\Release\ProcessExtensions.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Plug-And-Backup-Jobrunner\bin\Release\Plug-And-Backup-Jobrunner.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\Plug-And-Backup-Jobrunner\bin\Release\RestSharp.dll"; DestDir: "{app}"
; Source: "..\Plug-And-Backup-Restore\bin\Release\Plug-And-Backup-Restore.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\USBDLM-Backup\bin\Release\wingup\*"; DestDir: "{app}\wingup"; Flags: recursesubdirs ignoreversion
Source: "..\USBDLM-Backup\media-flash-usbpendrive.ico"; DestDir: "{app}"
Source: ".\include\7-ZipPortable\*"; DestDir: "{app}\7-ZipPortable"; Flags: recursesubdirs

[Icons]
Name: "{group}\Plug-And-Backup\Plug-And-Backup"; Filename: "{app}\Plug-And-Backup.exe"; WorkingDir: "{app}"; IconFilename: "{app}\media-flash-usbpendrive.ico"; IconIndex: 0

[Languages]
; Name: "de"; MessagesFile: "C:\Data\markus\Program Files (x86)\Inno Setup 5\Languages\German.isl"; LicenseFile: "..\USBDLM-Backup\bin\Release\MIT-license.rtf"
Name: "de"; MessagesFile: "D:\Program Files (x86)\Inno Setup 5\Languages\German.isl"; LicenseFile: "..\USBDLM-Backup\bin\Release\MIT-license.rtf"

[Setup]
AppName=Plug-And-Backup
AppVersion=1.6.1
AppCopyright=Markus Busche
AppId={{67BB3221-9878-4194-8816-998D79ADA364}
LicenseFile=..\USBDLM-Backup\MIT-license.rtf
DisableProgramGroupPage=yes
AppPublisher=Markus Busche
AppPublisherURL=https://plug-and-backup.eu
AppSupportURL=https://plug-and-backup.eu/help.html
AppContact=pab@butenostfreesen.de
MinVersion=0,6.0
DefaultDirName="{pf}\Plug-And-Backup"
OutputBaseFilename=Plug-And-Backup_Setup
AlwaysShowDirOnReadyPage=True

[UninstallDelete]
Type: filesandordirs; Name: "{app}"
Type: filesandordirs; Name: "{commonappdata}\Plug-And-Backup"

[UninstallRun]
Filename: "sc.exe"; Parameters: "stop PlugAndBackupService"; WorkingDir: "{win}"; StatusMsg: "Stopping service..."
Filename: "sc.exe"; Parameters: "delete PlugAndBackupService"; WorkingDir: "{win}"; StatusMsg: "Deleting service..."

[Run]
Filename: "sc.exe"; Parameters: "create PlugAndBackupService start=auto binPath=""{app}\Plug-And-Backup-Service.exe"""; StatusMsg: "Installing service..."
Filename: "sc.exe"; Parameters: "start PlugAndBackupService"; WorkingDir: "{win}"; StatusMsg: "Starting service..."

[Code]
procedure StopService();

var
  ResultCode: integer;
begin
  Exec('sc.exe', 'stop PlugAndBackupService', '', SW_HIDE, ewWaitUntilTerminated, ResultCode)
end;