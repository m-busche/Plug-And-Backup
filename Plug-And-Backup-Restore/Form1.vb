﻿Imports System.IO

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnRestore.Enabled = False
        btnSevenzipFM.Enabled = False
        ' Read device ID
        Try
            Dim sDeviceIDs() As String = Directory.GetFiles(".\", "PaB_Device-ID_*.txt")
            Dim sDeviceID = sDeviceIDs(0).Split("_")(2).Replace(".txt", "")
            If sDeviceID <> Nothing Then
                Me.Text = Me.Text + " - Device " + sDeviceID
            End If
        Catch ex As Exception

        End Try

        ' Read backup directories
        Try
            For Each sDirectory In Directory.GetDirectories(".\Plug-And-Backup\")
                sDirectory = sDirectory.Split("\")(2)
                cbBackupsets.Items.Add(sDirectory)
            Next
        Catch ex As Exception

        End Try
        If cbBackupsets.Items.Count = 0 Then
            MsgBox("No backup set(s) found on this device!", MsgBoxStyle.Critical)
        End If
    End Sub

    Private Sub btnSelectPath_Click(sender As Object, e As EventArgs) Handles btnSelectPath.Click
        ' Select restore directory
        Dim ofd As OpenFileDialog = New OpenFileDialog
        With ofd
            .CheckPathExists = True
            .CheckFileExists = False
            .FileName = "ignore.me"
            .InitialDirectory = Environment.SpecialFolder.MyComputer
        End With
        Dim result As DialogResult = ofd.ShowDialog
        If result = DialogResult.OK Then
            txtDestination.Text = Path.GetDirectoryName(ofd.FileName)
        End If
    End Sub

    ''' <summary>
    ''' Start restore process
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnRestore_Click(sender As Object, e As EventArgs) Handles btnRestore.Click
        If txtDestination.Text = "" Then
            MsgBox("Select a target directory for the recovery.", MsgBoxStyle.Information)
            Exit Sub
        End If
        If Not Directory.Exists(txtDestination.Text) Then
            Try
                Directory.CreateDirectory(txtDestination.Text)
            Catch ex As Exception
                MsgBox("Target directory didn't exist, creating it failed.", MsgBoxStyle.Critical)
                Exit Sub
            End Try
        End If
        If Not File.Exists(".\7-ZipPortable\App\7-Zip\7z.exe") Then
            MsgBox("7-Zip executable is missing on this backup device. Download and install 7-Zip to recover your files.", MsgBoxStyle.Critical)
            Exit Sub
        End If
        If (Directory.GetFileSystemEntries(txtDestination.Text).Length > 1) Then
            Dim result As DialogResult = MsgBox("The target directory is not empty. Extracting into an empty directory is recommended to " +
                                                "protect you from accidentally overwriting files." + vbCrLf + vbCrLf +
                                                "Do yout really want to extract to '" + txtDestination.Text + "'?", MsgBoxStyle.OkCancel)
            If result = DialogResult.Cancel Then
                Exit Sub
            End If
        End If


        Dim prSevenZip As Process = New Process
        Dim sSevenZipFiles As String = ".\Plug-And-Backup\" + cbBackupsets.Text + "\*.7z"

        With prSevenZip.StartInfo
            .FileName = ".\7-ZipPortable\App\7-Zip\7z.exe"
            .Arguments = String.Format("x ""{0}"" -o""{1}"" -r -aoa", sSevenZipFiles, txtDestination.Text)
        End With
        Try
            prSevenZip.Start()
        Catch ex As Exception
            MsgBox("Starting 7-Zip failed. Try to manually recover the archives using 7-Zip." + vbCrLf + ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub

    ''' <summary>
    ''' Star 7-Zip File Manager
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub btnSevenzipFM_Click(sender As Object, e As EventArgs) Handles btnSevenzipFM.Click
        Dim prSevenZip As Process = New Process

        If cbBackupsets.Text = "" Then
            MsgBox("Select a backup set first.", MsgBoxStyle.Information)
            Exit Sub
        End If

        With prSevenZip.StartInfo
            .FileName = ".\7-ZipPortable\7-ZipPortable.exe"
            If Directory.Exists(".\Plug-And-Backup\" + cbBackupsets.Text) Then
                .Arguments = ".\Plug-And-Backup\" + cbBackupsets.Text + "\"
            End If
        End With
        If Not File.Exists(prSevenZip.StartInfo.FileName) Then
            MsgBox("7-Zip executable is missing on this backup device. Download and install 7-Zip to recover your files.", MsgBoxStyle.Critical)
        Else
            prSevenZip.Start()
        End If
    End Sub

    ''' <summary>
    ''' Enable / disable restore button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub txtDestination_TextChanged(sender As Object, e As EventArgs) Handles txtDestination.TextChanged
        If cbBackupsets.Text <> "" And txtDestination.Text <> "" Then
            btnRestore.Enabled = True
        Else
            btnRestore.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Enable / disable restore and 7-Zip button
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub cbBackupsets_TextChanged(sender As Object, e As EventArgs) Handles cbBackupsets.TextChanged
        If cbBackupsets.Text <> "" And txtDestination.Text <> "" Then
            btnRestore.Enabled = True
        Else
            btnRestore.Enabled = False
        End If
        If cbBackupsets.Text <> "" Then
            btnSevenzipFM.Enabled = True
        Else
            btnSevenzipFM.Enabled = False
        End If
    End Sub

    ''' <summary>
    ''' Start 7-Zip -t in command window
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Private Sub TestSelectedBackupToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TestSelectedBackupToolStripMenuItem.Click
        If cbBackupsets.Text <> "" Then
            Dim sBasepath As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location)
            sBasepath = Path.GetPathRoot(sBasepath)
            Dim sArchiveFilename As String = ".\Plug-And-Backup\" + cbBackupsets.Text + "\*.7z"
            Dim sArgs As String = String.Format("/k {0}7-ZipPortable\App\7-Zip\7z.exe t ""{1}*"" -r", sBasepath, sArchiveFilename)

            Dim prSevenZip As Process = New Process
            With prSevenZip.StartInfo
                .FileName = "cmd.exe"
                .Arguments = sArgs
            End With
            prSevenZip.Start()
        Else
            MsgBox("Select a backup archive first.", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        About.ShowDialog()
    End Sub
End Class
