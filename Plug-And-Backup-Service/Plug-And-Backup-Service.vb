﻿Imports System.Management
Imports System.Runtime.InteropServices
Imports NLog
Imports NLog.Targets
Imports NLog.Config
Imports MadMilkman.Ini
Imports System.IO
Imports murrayju.ProcessExtensions

Public Enum ServiceState
    SERVICE_STOPPED = 1
    SERVICE_START_PENDING = 2
    SERVICE_STOP_PENDING = 3
    SERVICE_RUNNING = 4
    SERVICE_CONTINUE_PENDING = 5
    SERVICE_PAUSE_PENDING = 6
    SERVICE_PAUSED = 7
End Enum

<StructLayout(LayoutKind.Sequential)>
Public Structure ServiceStatus
    Public dwServiceType As Long
    Public dwCurrentState As ServiceState
    Public dwControlsAccepted As Long
    Public dwWin32ExitCode As Long
    Public dwServiceSpecificExitCode As Long
    Public dwCheckPoint As Long
    Public dwWaitHint As Long
End Structure

Public Class PlugAndBackupService
    Private Shared logger As Logger = LogManager.GetCurrentClassLogger()
    Private WithEvents m_MediaConnectWatcher As ManagementEventWatcher
    Private WithEvents f_Filesystemwatcher As FileSystemWatcher
    Public USBDriveName As String
    Public USBDriveLetter As String
    Declare Auto Function SetServiceStatus Lib "advapi32.dll" (ByVal handle As IntPtr, ByRef serviceStatus As ServiceStatus) As Boolean
    Public sDatapath As String = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) + "\Plug-And-Backup"
    Public sPaBINIFile As String = sDatapath + "\PaB.ini"

    Public Sub New()
        ' This call is required by the designer.
        InitializeComponent()
        Dim config = New LoggingConfiguration()
        Dim fileTarget = New FileTarget()
        config.AddTarget("file", fileTarget)
        fileTarget.Layout = "${date:format=HH\:mm\:ss} ${logger} ${message}"
        fileTarget.FileName = "${specialfolder:folder=CommonApplicationData}\Plug-And-Backup\logs\Service_${shortdate}.log"
        Dim rule = New LoggingRule("*", LogLevel.Debug, fileTarget)
        config.LoggingRules.Add(rule)
        LogManager.Configuration = config
    End Sub

    Public Sub _StartDetection()
        logger.Info("Starting drive detection")
        ' __InstanceOperationEvent will trap both Creation and Deletion of class instances
        Dim query2 As New WqlEventQuery("SELECT * FROM __InstanceOperationEvent WITHIN 1 " _
  + "WHERE TargetInstance ISA 'Win32_DiskDrive'")

        m_MediaConnectWatcher = New ManagementEventWatcher
        m_MediaConnectWatcher.Query = query2
        m_MediaConnectWatcher.Start()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Debugger starten
        ' Debugger.Launch()
        logger.Debug("INI file: " + sPaBINIFile)
        logger.Debug("Data path: " + sDatapath)

        Dim serviceStatus As ServiceStatus = New ServiceStatus()
        serviceStatus.dwCurrentState = ServiceState.SERVICE_START_PENDING
        serviceStatus.dwWaitHint = 100000
        SetServiceStatus(ServiceHandle, serviceStatus)
        serviceStatus.dwCurrentState = ServiceState.SERVICE_RUNNING
        SetServiceStatus(ServiceHandle, serviceStatus)
        Dim sVersion As String = String.Format("Version {0}", My.Application.Info.Version.ToString)
        logger.Info("Starting service " + sVersion)
        _deleteOldLogfiles()
        _StartDetection()
        _StartFileSystemWatcher()
    End Sub

    Private Sub _StartFileSystemWatcher()
        Dim fsw As FileSystemWatcher = New FileSystemWatcher
        fsw.Path = sDatapath
        fsw.Filter = "*.done"
        fsw.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)
        ' Add event handlers.
        ' AddHandler fsw.Changed, AddressOf _JobDone
        AddHandler fsw.Created, AddressOf _JobDone
        ' AddHandler fsw.Deleted, AddressOf _JobDone
        ' AddHandler fsw.Renamed, AddressOf _JobDone
        fsw.EnableRaisingEvents = True
    End Sub

    Private Sub _deleteOldLogfiles()
        Dim sLogPath As String = sDatapath + "\logs"
        For Each file As String In Directory.GetFiles(sLogPath)
            Dim fi As New FileInfo(file)
            If fi.LastAccessTime < DateTime.Now.AddDays(-7) Then
                Try
                    logger.Debug("Deleting stale log file: " + fi.Name)
                    fi.Delete()
                Catch ex As Exception
                    logger.Warn("Deleting stale log file failed: " + fi.Name)
                End Try
            End If
        Next
    End Sub

    Protected Overrides Sub OnStop()
        _deleteOldLogfiles()
    End Sub

    Private Sub Arrived(ByVal sender As Object, ByVal e As System.Management.EventArrivedEventArgs) Handles m_MediaConnectWatcher.EventArrived
        logger.Debug("New drive arrived or removed.")
        Dim mbo, obj As ManagementBaseObject

        ' the first thing we have to do is figure out if this is a creation or deletion event
        mbo = e.NewEvent
        ' next we need a copy of the instance that was either created or deleted
        obj = CType(mbo("TargetInstance"), ManagementBaseObject)

        Select Case mbo.ClassPath.ClassName
            Case "__InstanceCreationEvent"
                logger.Info("Device created.")
                If obj("InterfaceType") = "USB" Then
                    logger.Debug(obj("Caption") & " (Drive letter " & GetDriveLetterFromDisk(obj("Name")) & ") has been plugged in")
                Else
                    logger.Debug(obj("InterfaceType"))
                End If
                _GetBackupDeviceIDs(GetDriveLetterFromDisk(obj("Name")))
            Case "__InstanceDeletionEvent"
                logger.Info("Device removed.")
                If obj("InterfaceType") = "USB" Then
                    logger.Debug(obj("Caption") & " has been unplugged")
                    If obj("Caption") = USBDriveName Then
                        USBDriveLetter = ""
                        USBDriveName = ""
                    End If
                Else
                    logger.Debug(obj("InterfaceType"))
                End If
            Case Else
                logger.Debug("nope: " & obj("Caption"))
        End Select
    End Sub

    Private Sub _GetBackupDeviceIDs(sDriveletter)
        Dim mydevices As New List(Of USBDeviceInfo)()
        mydevices = GetUSBDevices()
        Dim options As IniOptions = New IniOptions()
        Dim IniFile As IniFile = New IniFile(options)
        Dim sBatchfile As String = Nothing

        Try
            IniFile.Load(sPaBINIFile)
        Catch ex As FileNotFoundException
            logger.Warn("INI file not found!")
            Exit Sub
        Catch ex2 As Exception
            logger.Warn(ex2.Message)
        End Try

        Using stream As Stream = File.OpenRead(sPaBINIFile)
            IniFile.Load(stream)
        End Using

        ' Get PrePostCommandWait from INI
        Dim iWaitInterval As Integer = 10000
        Try
            Dim settings As IniSection = IniFile.Sections("settings")
            iWaitInterval = settings.Keys("PrePostCommandWait").Value * 1000
            logger.Debug("Setting PrePostCommandWait to " + iWaitInterval.ToString)
        Catch ex As Exception

        End Try

        For Each section As IniSection In IniFile.Sections
            If section.Name.StartsWith("Device_") Then
                Dim deviceid = section.Name.Split("_")(1)
                For Each udevice In mydevices
                    If udevice.DeviceID.ToString.Contains(deviceid) Then
                        logger.Debug("*** Device ID matched: " + udevice.DeviceID.ToString)
                        Try
                            Dim sPreCmd As String = section.Keys("PreCmd").Value
                            If sPreCmd <> "" Then
                                logger.Info("This backup has a PreCmd")
                                _ProcessPrePostCMD(sPreCmd, iWaitInterval)
                            Else
                                logger.Info("PreCmd not set")
                            End If
                        Catch ex As Exception
                            logger.Info("No PreCmd key found")
                        End Try
                        _LaunchBackup(udevice.DeviceID.ToString, sDriveletter)
                        Exit Sub
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub _ProcessPrePostCMD(sCMD As String, iWaitMax As Integer)
        logger.Debug("Pre/Post command from ini")
        Dim prProcess As Process = New Process
        Dim sfilename As String = ""
        Dim sArgs As String = ""
        Try
            sfilename = sCMD.Split(" ")(0)
        Catch ex As Exception
            logger.Warn("Unable to extract Pre/Post command")
            Exit Sub
        End Try
        logger.Debug("Pre/Post command executable: " + sfilename)
        If sfilename <> "" Then
            Try
                For i As Integer = 1 To sCMD.Split(" ").Count - 1
                    sArgs = sArgs + " " + sCMD.Split(" ")(i)
                Next
                sArgs = sArgs.Trim
                logger.Debug("Pre/Post command arguments: " + sArgs)
            Catch ex As Exception
                logger.Debug("No arguments found")
            End Try
        End If

        With prProcess.StartInfo
            .FileName = sfilename
            If sArgs <> "" Then
                .Arguments = sArgs
            End If
        End With
        Try
            logger.Debug("Launching process")
            prProcess.Start()
            If iWaitMax > 1000 Then
                logger.Debug("Waiting " + iWaitMax.ToString + "ms for termination.")
                prProcess.WaitForExit(iWaitMax)
            Else
                prProcess.WaitForExit()
            End If

            Try
                Dim sEcode As String = prProcess.ExitCode.ToString
                logger.Info("Pre/Post command exit code: " + prProcess.ExitCode.ToString)
            Catch ex2 As Exception
                logger.Warn("Pre/Post command ran into timeout (" + iWaitMax.ToString + "ms)")
        End Try
        Catch ex1 As Exception
            logger.Warn("Pre/Post command failed to start:" + ex1.Message)
        End Try
    End Sub

    Private Function GetDriveLetterFromDisk(ByVal Name As String) As String
        Dim oq_part, oq_disk As ObjectQuery
        Dim mos_part, mos_disk As ManagementObjectSearcher
        Dim obj_part, obj_disk As ManagementObject
        Dim ans As String = ""

        ' WMI queries use the "\" as an escape charcter
        Name = Replace(Name, "\", "\\")

        ' First we map the Win32_DiskDrive instance with the association called
        ' Win32_DiskDriveToDiskPartition. Then we map the Win23_DiskPartion
        ' instance with the assocation called Win32_LogicalDiskToPartition

        oq_part = New ObjectQuery("ASSOCIATORS OF {Win32_DiskDrive.DeviceID=""" & Name & """} WHERE AssocClass = Win32_DiskDriveToDiskPartition")
        mos_part = New ManagementObjectSearcher(oq_part)
        For Each obj_part In mos_part.Get()

            oq_disk = New ObjectQuery("ASSOCIATORS OF {Win32_DiskPartition.DeviceID=""" & obj_part("DeviceID") & """} WHERE AssocClass = Win32_LogicalDiskToPartition")
            mos_disk = New ManagementObjectSearcher(oq_disk)
            For Each obj_disk In mos_disk.Get()
                ans &= obj_disk("Name") & ","
            Next
        Next
        Return ans.Trim(","c)
    End Function


    Private Shared Function GetUSBDevices() As List(Of USBDeviceInfo)
        Dim devices As New List(Of USBDeviceInfo)()

        Dim collection As ManagementObjectCollection
        Using searcher = New ManagementObjectSearcher("Select * From Win32_USBHub")
            collection = searcher.[Get]()
        End Using

        For Each device In collection
            devices.Add(New USBDeviceInfo(DirectCast(device.GetPropertyValue("DeviceID"), String), DirectCast(device.GetPropertyValue("PNPDeviceID"), String), DirectCast(device.GetPropertyValue("Description"), String)))
        Next

        collection.Dispose()
        Return devices
    End Function

    Private Sub _LaunchBackup(ByVal sDeviceID As String, ByVal sDriveletter As String)
        sDriveletter = sDriveletter.ToString.Substring(0, 1)
        sDeviceID = sDeviceID.Split("\")(2)
        _WriteJobDetails(sDeviceID, sDriveletter)
        Dim sJobrunner As String = Path.GetDirectoryName(Reflection.Assembly.GetExecutingAssembly().Location) + "\Plug-And-Backup-Jobrunner.exe"
        logger.Debug(String.Format("Launching {0}", sJobrunner))
        ProcessExtensions.StartProcessAsCurrentUser(sJobrunner)
    End Sub

    Private Function _WriteJobDetails(sDeviceID As String, sDriveletter As String) As Boolean
        logger.Debug("Writing launch information for Jobrunner: " + sDeviceID + " " + sDriveletter)
        Try
            Using sw As StreamWriter = New StreamWriter(sDatapath + "\launch.txt")
                sw.WriteLine(sDeviceID)
                sw.WriteLine(sDriveletter)
            End Using
            Return True
        Catch ex As Exception
            logger.Warn("Failed writing launch information for Jobrunner: " + ex.Message)
            Return False
        End Try
    End Function

    Private Sub _JobDone(ByVal sender As Object, ByVal e As FileSystemEventArgs) Handles f_Filesystemwatcher.Created
        If e.Name.ToLower.StartsWith("job_") And e.Name.EndsWith(".done") Then
            Dim sDeviceID As String = e.Name
            logger.Info("Backup job done, checking for post command")
            Dim sDoneFileName As String = sDatapath + "\" + sDeviceID
            ' Delete done file
            Try
                logger.Debug("Deleting job file: " + sDoneFileName)
                File.Delete(sDoneFileName)
            Catch ex As Exception
                logger.Warn("Failed deleting " + sDoneFileName)
            End Try

            ' Extract device id from file name
            Try
                sDeviceID = sDeviceID.Split("_")(1).Split(".")(0)
            Catch ex As Exception
                logger.Warn("Failed extracting device id from file name")
                Exit Sub
            End Try
            logger.Debug("Post command device ID: " + sDeviceID)

            ' Read ini file, find matching section and get PostCmd value
            Dim options As IniOptions = New IniOptions()
            Dim IniFile As IniFile = New IniFile(options)
            Dim sBatchfile As String = Nothing

            Try
                IniFile.Load(sPaBINIFile)
            Catch ex As FileNotFoundException
                logger.Warn("Post command : INI file not found!")
                Exit Sub
            Catch ex2 As Exception
                logger.Warn(ex2.Message)
            End Try

            For Each section As IniSection In IniFile.Sections
                If section.Name = "Device_" + sDeviceID Then
                    Try
                        Dim sCmd As String = section.Keys("PostCmd").Value
                        If sCmd <> "" Then
                            logger.Info("Found matching post command")
                            _ProcessPrePostCMD(sCmd, 10000)
                        Else
                            logger.Info("Post command empty")
                        End If
                    Catch ex As Exception
                        logger.Debug("No post command found")
                    End Try
                End If
            Next
            'Else
            '    logger.Info("Triggered another file: " + e.Name)
        End If
    End Sub
End Class
